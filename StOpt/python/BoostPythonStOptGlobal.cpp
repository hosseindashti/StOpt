// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)

/** \file BoostPythonGrids.cpp
 * \brief Map grids  classes to python
 * \author Xavier Warin
 */
#include <Eigen/Dense>
#include <iostream>
#include <memory>
#include <functional>
#include <boost/bind.hpp>
#include <boost/version.hpp>
#include <boost/python/converter/registry.hpp>
#include "geners/BinaryFileArchive.hh"
#include "geners/StringArchive.hh"
#include "StOpt/core/grids/SpaceGrid.h"
#include "StOpt/core/grids/FullGrid.h"
#include "StOpt/core/utils/StateWithStocks.h"
#include "StOpt/regression/BaseRegression.h"
#include "StOpt/dp/SimulatorDPBase.h"
#include "StOpt/dp/OptimizerDPBase.h"
#include "StOpt/dp/TransitionStepRegressionDP.h"
#include "StOpt/dp/FinalStepRegressionDP.h"
#include "StOpt/dp/SimulateStepRegression.h"
#include "StOpt/dp/SimulateStepRegressionControl.h"
#include "StOpt/semilagrangien/SemiLagrangEspCond.h"
#ifdef USE_MPI
#include "StOpt/dp/TransitionStepRegressionDPDist.h"
#include "StOpt/dp/FinalStepRegressionDPDist.h"
#include "StOpt/dp/SimulateStepRegressionDist.h"
#include "StOpt/dp/SimulateStepRegressionControlDist.h"
#include "StOpt/core/parallelism/reconstructProc0Mpi.h"
#endif
#include "StOpt/python/PayOffBase.h"

using namespace Eigen;
using namespace StOpt;
using namespace gs;

#ifdef __linux__
#ifdef __clang__
#if BOOST_VERSION <  105600
// map std::shared ptr to boost python
namespace boost
{
template<class T> T *get_pointer(std::shared_ptr<T> p)
{
    return p.get();
}
}
#endif
#endif
#endif

template<typename T>
void do_release(typename boost::shared_ptr<T> const &, T *)
{
}

#ifdef _DEBUG
#undef _DEBUG
#include <Python.h>
#define _DEBUG
#else
#include <Python.h>
#endif
#include <numpy/arrayobject.h>
#include "StOpt/python/NumpyConverter.hpp"
#include "StOpt/python/VectorAndList.h"
#include "StOpt/python/PairConverter.h"

using namespace boost::python;



template<typename T>
typename std::shared_ptr<T> make_shared_ptr(typename boost::shared_ptr<T> const &p)
{
    return
        std::shared_ptr<T>(
            p.get(),
            boost::bind(&do_release<T>, p, _1));

}

//Wrapper for simulator
//**********************
struct SimulatorDPBaseWrap : SimulatorDPBase, wrapper< SimulatorDPBase>
{
    MatrixXd getParticles() const
    {
        return  this->get_override("getParticles")();
    }
    void stepForward()
    {
        this->get_override("stepForward")();
    }
    void stepBackward()
    {
        this->get_override("stepBackward")();
    }
    MatrixXd  stepForwardAndGetParticles()
    {
        return this->get_override("stepForwardAndGetParticles")();
    }
    MatrixXd  stepBackwardAndGetParticles()
    {
        return this->get_override("stepBackwardAndGetParticles")();
    }
    int getDimension() const
    {
        return this->get_override("getDimension")();
    }
    int getNbStep() const
    {
        return this->get_override("getNbStep")();
    }
    double getStep() const
    {
        return this->get_override("getStep")();
    }
    int getNbSimul() const
    {
        return this->get_override("getNbSimul")();
    }
    double getCurrentStep() const
    {
        return this->get_override("getCurrentStep")();
    }
    double  getActuStep() const
    {
        return this->get_override("getActuStep")();
    }
    double  getActu() const
    {
        return this->get_override("getActu")();
    }
};


// wrapper for optimization object for Optimization Base object for dynamic programming
//*************************************************************************************
struct  OptimizerDPBaseWrap : OptimizerDPBase, wrapper< OptimizerDPBase>
{

    std::vector< std::array< double, 2> > getCone(const  std::vector<  std::array< double, 2>  > &p_regionByProcessor) const
    {
        return  this->get_override("getCone")(p_regionByProcessor);
    }

    Eigen::Array< bool, Eigen::Dynamic, 1> getDimensionToSplit() const
    {
        return this->get_override("getDimensionToSplit")() ;
    }
    std::pair< ArrayXXd, ArrayXXd>   stepOptimize(const   std::shared_ptr< StOpt::SpaceGrid> &p_grid, const ArrayXd   &p_stock, const std::vector<StOpt::ContinuationValue> &p_condEsp,
            const std::vector < std::shared_ptr< Eigen::ArrayXXd > > &p_phiIn) const
    {
        return	this->get_override("stepOptimize")(p_grid, p_stock, p_condEsp, p_phiIn);
    }

    void stepSimulate(const std::shared_ptr< StOpt::SpaceGrid>   &p_grid, const std::vector< StOpt::GridAndRegressedValue  > &p_continuation,
                      StOpt::StateWithStocks &p_state,
                      Eigen::Ref<Eigen::ArrayXd> p_phiInOut) const
    {
        this->get_override("stepSimulate")(p_grid, p_continuation, p_state, p_phiInOut);
    }

    void stepSimulateControl(const std::shared_ptr< StOpt::SpaceGrid>   &p_grid, const std::vector< StOpt::GridAndRegressedValue  > &p_control,
                             StOpt::StateWithStocks &p_state,
                             Eigen::Ref<Eigen::ArrayXd> p_phiInOut) const
    {
        this->get_override("stepSimulateControl")(p_grid, p_control, p_state, p_phiInOut);
    }

    int getNbRegime() const
    {
        return this->get_override("getNbRegime")();
    }

    int getNbControl() const
    {
        return this->get_override("getNbControl")();
    }

    std::shared_ptr< StOpt::SimulatorDPBase > getSimulator() const
    {
        return  this->get_override("getSimulator")();
    }
    void setSimulator(std::shared_ptr< StOpt::SimulatorDPBase >)
    {
        this->get_override("setSimulator")();
    }
    int getSimuFuncSize() const
    {
        return this->get_override("getSimuFuncSize")();
    }
};

// wrapper for Final Step
//***********************
class FinalStepRegressionDPWrap: FinalStepRegressionDP
{
public :
    FinalStepRegressionDPWrap(const  boost::shared_ptr<SpaceGrid> &p_pGridCurrent, const int &p_nbRegime):
        FinalStepRegressionDP(make_shared_ptr<SpaceGrid>(p_pGridCurrent), p_nbRegime) {}

    std::vector< std::shared_ptr< ArrayXXd > >  operator()(object &p_funcValue, const ArrayXXd &p_particles) const
    {
        auto lambda = [p_funcValue](const int &p_i, const ArrayXd & p_x, const ArrayXd & p_y)->double { return boost::python::extract<double>(p_funcValue(p_i, p_x, p_y));};

        return FinalStepRegressionDP::operator()(std::function<double(const int &, const ArrayXd &, const ArrayXd &)> (lambda), p_particles);
    }

    std::vector< std::shared_ptr< ArrayXXd > >  set(const  boost::shared_ptr<PayOffBase>   &p_funcValue, const ArrayXXd &p_particles) const
    {
        return FinalStepRegressionDP::operator()(p_funcValue->getFunction(), p_particles);
    }
};

// wrapper for Transition step
//*****************************
class TransitionStepRegressionDPWrap: TransitionStepRegressionDP
{
public :
    TransitionStepRegressionDPWrap(const boost::shared_ptr< SpaceGrid >   &p_gridCurrent ,  const boost::shared_ptr< SpaceGrid >   &p_gridPrevious, const  boost::shared_ptr< OptimizerDPBase>   &p_optimizer):
        TransitionStepRegressionDP(make_shared_ptr<FullGrid>(boost::dynamic_pointer_cast<FullGrid>(p_gridCurrent)),
                                   make_shared_ptr<FullGrid>(boost::dynamic_pointer_cast<FullGrid>(p_gridPrevious)), make_shared_ptr<OptimizerDPBase>(p_optimizer)) {  }

    std::pair< std::vector< std::shared_ptr< Eigen::ArrayXXd > >, std::vector<  std::shared_ptr<  ArrayXXd > > >  oneStepWrap(const boost::python::list &p_phiIn,   const boost::shared_ptr< BaseRegression>     &p_condExp) const
    {
        return TransitionStepRegressionDP::oneStep(convertFromListShPtr<ArrayXXd>(p_phiIn), make_shared_ptr(p_condExp));
    }

    void dumpContinuationValuesWrap(boost::shared_ptr<gs::BinaryFileArchive>  p_ar , const std::string &p_name, const int &p_iStep, const  boost::python::list  &p_phiIn,  const  boost::python::list   &p_control, const   boost::shared_ptr<BaseRegression>    &p_condExp) const
    {
        TransitionStepRegressionDP::dumpContinuationValues(make_shared_ptr<gs::BinaryFileArchive>(p_ar), p_name, p_iStep, convertFromListShPtr<ArrayXXd>(p_phiIn),
                convertFromListShPtr<ArrayXXd>(p_control), make_shared_ptr(p_condExp));
    }
};


// Wrapper for SimulateStepRegression
class SimulateStepRegressionWrap : SimulateStepRegression

{
public :
    SimulateStepRegressionWrap(gs::BinaryFileArchive &p_ar,  const int &p_iStep,  const std::string &p_nameCont,
                               const   boost::shared_ptr< SpaceGrid > &p_pGridFollowing,
                               const  boost::shared_ptr< OptimizerDPBase > &p_pOptimize):
        SimulateStepRegression(p_ar, p_iStep, p_nameCont, make_shared_ptr<SpaceGrid>(p_pGridFollowing), make_shared_ptr<OptimizerDPBase >(p_pOptimize))
    {}

    std::pair< std::vector<StateWithStocks>,  ArrayXXd >  oneStepWrap(const boost::python::list    &p_statevector,  const ArrayXXd   &p_phiInOut) const
    {
        std::vector<StateWithStocks> stateLoc = convertFromList<StateWithStocks>(p_statevector);
        ArrayXXd phiLoc(p_phiInOut);
        SimulateStepRegression::oneStep(stateLoc, phiLoc);
        return make_pair(stateLoc, phiLoc) ;
    }
};


// Wrapper for SimulateStepRegressionControl
class SimulateStepRegressionControlWrap : SimulateStepRegressionControl

{
public :
    SimulateStepRegressionControlWrap(gs::BinaryFileArchive &p_ar,  const int &p_iStep,  const std::string &p_nameCont,
                                      const   boost::shared_ptr< SpaceGrid > &p_pGridFollowing, const  boost::shared_ptr< OptimizerDPBase > &p_pOptimize):
        SimulateStepRegressionControl(p_ar, p_iStep, p_nameCont, make_shared_ptr<SpaceGrid>(p_pGridFollowing), make_shared_ptr<OptimizerDPBase >(p_pOptimize))
    {}

    std::pair< std::vector<StateWithStocks>,  ArrayXXd >  oneStepWrap(const boost::python::list    &p_statevector,  const ArrayXXd   &p_phiInOut) const
    {
        std::vector<StateWithStocks> stateLoc = convertFromList<StateWithStocks>(p_statevector);
        ArrayXXd phiLoc(p_phiInOut);
        SimulateStepRegressionControl::oneStep(stateLoc, phiLoc);
        return std::make_pair(stateLoc, phiLoc) ;
    }
};



#ifdef USE_MPI

class FinalStepRegressionDPDistWrap: FinalStepRegressionDPDist
{
public :
    FinalStepRegressionDPDistWrap(const  boost::shared_ptr<SpaceGrid> &p_pGridCurrent, const int &p_nbRegime,
                                  const Eigen::Array< bool, Eigen::Dynamic, 1>   &p_bdimToSplit):
        FinalStepRegressionDPDist(make_shared_ptr<FullGrid>(boost::dynamic_pointer_cast<FullGrid>(p_pGridCurrent)), p_nbRegime, p_bdimToSplit) {}

    std::vector< std::shared_ptr< ArrayXXd > >  operator()(object &p_funcValue, const ArrayXXd &p_particles) const
    {
        auto lambda = [p_funcValue](const int &p_i, const ArrayXd & p_x, const ArrayXd & p_y)->double { return boost::python::extract<double>(p_funcValue(p_i, p_x, p_y));};
        return FinalStepRegressionDPDist::operator()(std::function<double(const int &, const ArrayXd &, const ArrayXd &)> (lambda), p_particles);
    }

    std::vector< std::shared_ptr< ArrayXXd > >  set(const  boost::shared_ptr<PayOffBase>   &p_funcValue, const ArrayXXd &p_particles) const
    {
        return FinalStepRegressionDPDist::operator()(p_funcValue->getFunction(), p_particles);
    }
};

class TransitionStepRegressionDPDistWrap: TransitionStepRegressionDPDist
{
public :
    TransitionStepRegressionDPDistWrap(const boost::shared_ptr< SpaceGrid >   &p_gridCurrent ,  const boost::shared_ptr< SpaceGrid >   &p_gridPrevious, const  boost::shared_ptr< OptimizerDPBase>   &p_optimizer):
        TransitionStepRegressionDPDist(make_shared_ptr<FullGrid>(boost::dynamic_pointer_cast<FullGrid>(p_gridCurrent)),
                                       make_shared_ptr<FullGrid>(boost::dynamic_pointer_cast<FullGrid>(p_gridPrevious)),
                                       make_shared_ptr<OptimizerDPBase>(p_optimizer)) {  }

    std::pair< std::vector< std::shared_ptr< Eigen::ArrayXXd > >, std::vector<  std::shared_ptr<  ArrayXXd > > > oneStepWrap(const boost::python::list &p_phiIn,   const boost::shared_ptr< BaseRegression>     &p_condExp) const
    {
        return TransitionStepRegressionDPDist::oneStep(convertFromListShPtr<ArrayXXd>(p_phiIn), make_shared_ptr(p_condExp));
    }

    void dumpContinuationValuesWrap(boost::shared_ptr<gs::BinaryFileArchive> p_ar , const std::string &p_name, const int &p_iStep, const  boost::python::list  &p_phiIn,
                                    const  boost::python::list   &p_control, const   boost::shared_ptr<BaseRegression>    &p_condExp,
                                    const bool &p_bOneFile) const
    {
        TransitionStepRegressionDPDist::dumpContinuationValues(make_shared_ptr<gs::BinaryFileArchive>(p_ar), p_name, p_iStep, convertFromListShPtr<ArrayXXd>(p_phiIn),
                convertFromListShPtr<ArrayXXd>(p_control), make_shared_ptr(p_condExp), p_bOneFile);
    }
};

class SimulateStepRegressionDistWrap : SimulateStepRegressionDist

{
public :
    SimulateStepRegressionDistWrap(gs::BinaryFileArchive &p_ar,  const int &p_iStep,  const std::string &p_nameCont,
                                   const   boost::shared_ptr< SpaceGrid > &p_pGridFollowing, const  boost::shared_ptr< OptimizerDPBase > &p_pOptimize,
                                   const bool &p_bOneFile):
        SimulateStepRegressionDist(p_ar, p_iStep, p_nameCont, make_shared_ptr<FullGrid>(boost::dynamic_pointer_cast<FullGrid>(p_pGridFollowing)),
                                   make_shared_ptr<OptimizerDPBase >(p_pOptimize), p_bOneFile)
    {}

    std::pair< std::vector<StateWithStocks>,  ArrayXXd >  oneStepWrap(const boost::python::list    &p_statevector,  const ArrayXXd   &p_phiInOut) const
    {
        std::vector<StateWithStocks> stateLoc = convertFromList<StateWithStocks>(p_statevector);
        ArrayXXd phiLoc(p_phiInOut);
        SimulateStepRegressionDist::oneStep(stateLoc, phiLoc);
        return std::make_pair(stateLoc, phiLoc) ;
    }
};


class SimulateStepRegressionControlDistWrap : SimulateStepRegressionControlDist

{
public :
    SimulateStepRegressionControlDistWrap(gs::BinaryFileArchive &p_ar,  const int &p_iStep,  const std::string &p_nameCont,
                                          const   boost::shared_ptr< SpaceGrid > &p_pGridCurrent, const   boost::shared_ptr< SpaceGrid > &p_pGridFollowing,
                                          const  boost::shared_ptr< OptimizerDPBase > &p_pOptimize,
                                          const bool &p_bOneFile):
        SimulateStepRegressionControlDist(p_ar, p_iStep, p_nameCont, make_shared_ptr<FullGrid>(boost::dynamic_pointer_cast<FullGrid>(p_pGridCurrent)),
                                          make_shared_ptr<FullGrid>(boost::dynamic_pointer_cast<FullGrid>(p_pGridFollowing)),
                                          make_shared_ptr<OptimizerDPBase >(p_pOptimize), p_bOneFile)
    {}

    std::pair< std::vector<StateWithStocks>,  ArrayXXd >  oneStepWrap(const boost::python::list    &p_statevector,  const ArrayXXd   &p_phiInOut) const
    {
        std::vector<StateWithStocks> stateLoc = convertFromList<StateWithStocks>(p_statevector);
        ArrayXXd phiLoc(p_phiInOut);
        SimulateStepRegressionControlDist::oneStep(stateLoc, phiLoc);
        return std::make_pair(stateLoc, phiLoc) ;
    }
};

double  reconstructProc0MpiWrap(const Eigen::ArrayXd &p_point, const boost::shared_ptr< SpaceGrid> &p_grid, const  std::shared_ptr< Eigen::ArrayXXd >    &p_values,
                                const Eigen::Array< bool, Eigen::Dynamic, 1>   &p_bdimToSplit)
{
    return reconstructProc0Mpi(p_point, make_shared_ptr<FullGrid>(boost::dynamic_pointer_cast<FullGrid>(p_grid)), p_values, p_bdimToSplit);
}
#endif




/// \brief Encapsulation for regression module
BOOST_PYTHON_MODULE(StOptGlobal)
{

    using namespace boost::python;

    Register<ArrayXXd>();
    Register<ArrayXd>();
    Register<Array< bool, Dynamic, 1> >();


    // map geners
    //***********
    class_<BinaryFileArchive, boost::noncopyable>("BinaryFileArchive", init< const char *, const char * >())
    ;
    class_<StringArchive, boost::noncopyable>("StringArchive", init< >())
    ;

    class_<StateWithStocks>("StateWithStocks", init< const int &, const ArrayXd &, const ArrayXd &>())
    .def("getPtStock", &StateWithStocks::getPtStock, return_value_policy<copy_const_reference>()) // copy instead fo reference
    .def("getRegime", &StateWithStocks::getRegime)
    .def("setPtStock", &StateWithStocks::setPtStock)
    .def("setStochasticRealization", &StateWithStocks::setStochasticRealization)
    .def("getStochasticRealization", &StateWithStocks::getStochasticRealization, return_value_policy<copy_const_reference>())
    .def("setRegime", &StateWithStocks::setRegime)
    ;

    // All the converters
    //********************

    boost::python::type_info infoVecToListShPtr = boost::python::type_id< std::vector<std::shared_ptr< ArrayXXd > , std::allocator< std::shared_ptr< ArrayXXd > > > >();
    const boost::python::converter::registration *regShPtr = boost::python::converter::registry::query(infoVecToListShPtr);
    // check if already registered
    if (regShPtr == NULL)
    {
        // Vector of shared_ptr< Array>
        to_python_converter<std::vector<std::shared_ptr< ArrayXXd > , std::allocator< std::shared_ptr< ArrayXXd > > > , VecToListShPtr<  ArrayXXd > >();
    }
    else if ((*regShPtr).m_to_python == NULL)
    {
        to_python_converter<std::vector<std::shared_ptr< ArrayXXd > , std::allocator< std::shared_ptr< ArrayXXd > > > , VecToListShPtr<  ArrayXXd > >();
    }
    boost::python::type_info info = boost::python::type_id< std::pair< std::vector< StateWithStocks , std::allocator<StateWithStocks> > , ArrayXXd > >();
    const boost::python::converter::registration *reg = boost::python::converter::registry::query(info);
    // check if already registered
    if (reg == NULL)
    {
        to_python_converter<  std::pair< std::vector< StateWithStocks , std::allocator<StateWithStocks> > , ArrayXXd > , pairSpec< StateWithStocks,  ArrayXXd> >() ;
    }
    else if ((*reg).m_to_python == NULL)
    {
        to_python_converter<  std::pair< std::vector< StateWithStocks , std::allocator<StateWithStocks> > , ArrayXXd > , pairSpec< StateWithStocks,  ArrayXXd>  >() ;
    }

    boost::python::type_info info_ = boost::python::type_id< std::pair< Eigen::ArrayXXd, Eigen::ArrayXXd  > >();
    const boost::python::converter::registration *reg_ = boost::python::converter::registry::query(info_);
    // check if already registered
    if (reg_ == NULL)
    {
        to_python_converter<  std::pair< ArrayXXd, ArrayXXd  > , pairSpec1< ArrayXXd, ArrayXXd > >() ;
    }
    else if ((*reg_).m_to_python == NULL)
    {
        to_python_converter<  std::pair< ArrayXXd, ArrayXXd > , pairSpec1< ArrayXXd, ArrayXXd>  >() ;
    }

    boost::python::type_info info1 = boost::python::type_id< std::pair< std::vector< std::shared_ptr< Eigen::ArrayXXd > >, std::vector<  std::shared_ptr<  ArrayXXd > > > >();
    const boost::python::converter::registration *reg1 = boost::python::converter::registry::query(info1);
    // check if already registered
    if (reg1 == NULL)
    {
        to_python_converter<  std::pair< std::vector< std::shared_ptr< Eigen::ArrayXXd > >, std::vector<  std::shared_ptr<  ArrayXXd > > > , pairSpec2< ArrayXXd, ArrayXXd > >() ;
    }
    else if ((*reg1).m_to_python == NULL)
    {
        to_python_converter<  std::pair< std::vector< std::shared_ptr< Eigen::ArrayXXd > >, std::vector<  std::shared_ptr<  ArrayXXd > > > , pairSpec2< ArrayXXd, ArrayXXd > >() ;
    }


    // map simulator DP base
    //**********************
    class_<SimulatorDPBaseWrap, std::shared_ptr<SimulatorDPBaseWrap>, boost::noncopyable>("SimulatorDPBase")
    .def("getParticles", pure_virtual(&SimulatorDPBase::getParticles))
    .def("stepForward", pure_virtual(&SimulatorDPBase::stepForward))
    .def("stepBackward", pure_virtual(&SimulatorDPBase::stepBackward))
    .def("stepForwardAndGetParticles", pure_virtual(&SimulatorDPBase::stepForwardAndGetParticles))
    .def("stepBackwardAndGetParticles", pure_virtual(&SimulatorDPBase::stepBackwardAndGetParticles))
    .def("getDimension", pure_virtual(&SimulatorDPBase::getDimension))
    .def("getNbStep", pure_virtual(&SimulatorDPBase::getNbStep))
    .def("getStep", pure_virtual(&SimulatorDPBase::getStep))
    .def("getNbSimul", pure_virtual(&SimulatorDPBase::getNbSimul))
    .def("getActu", pure_virtual(&SimulatorDPBase::getActu))
    .def("getActuStep", pure_virtual(&SimulatorDPBase::getActuStep))
    ;

    // for shared_ptr<SimulatorDPBase>
    objects::class_value_wrapper< std::shared_ptr<SimulatorDPBase>, objects::make_ptr_instance<SimulatorDPBase, objects::pointer_holder<std::shared_ptr<SimulatorDPBase>, SimulatorDPBase> > >();


    // map optimizer DP base
    //**********************
    class_<OptimizerDPBaseWrap, std::shared_ptr<OptimizerDPBaseWrap>, boost::noncopyable>("OptimizerDPBase")
    .def("stepOptimize", pure_virtual(&OptimizerDPBase::stepOptimize))
    .def("stepSimulate", pure_virtual(&OptimizerDPBase::stepSimulate))
    .def("stepSimulateControl", pure_virtual(&OptimizerDPBase::stepSimulateControl))
    .def("getNbRegime", pure_virtual(&OptimizerDPBase::getNbRegime))
    .def("getNbControl", pure_virtual(&OptimizerDPBase::getNbControl))
    .def("getSimulator", pure_virtual(&OptimizerDPBase::getSimulator))
    .def("getSimuFuncSize", pure_virtual(&OptimizerDPBase::getSimuFuncSize))
    .def("getDimensionToSplit", pure_virtual(&OptimizerDPBase::getDimensionToSplit))
    ;

    // for shared_ptr<OptimizerDPBase>
    objects::class_value_wrapper< std::shared_ptr<OptimizerDPBase>, objects::make_ptr_instance<OptimizerDPBase, objects::pointer_holder<std::shared_ptr<OptimizerDPBase>, OptimizerDPBase> > >();

    // map final pay off
    //******************
    class_<PayOffBase , boost::noncopyable>("PayOffBase", no_init);

    // // maps final step
    class_<FinalStepRegressionDPWrap, boost::noncopyable >("FinalStepRegressionDP" , init< const boost::shared_ptr<SpaceGrid>  &, const int & >())
    .def("set", &FinalStepRegressionDPWrap::set)
    .def("_setItem_", &FinalStepRegressionDPWrap::operator())
    ;
    // maps Transition step in optimization
    class_<TransitionStepRegressionDPWrap, boost::noncopyable>("TransitionStepRegressionDP", init< const boost::shared_ptr<SpaceGrid> &,
            const  boost::shared_ptr<SpaceGrid> & , const boost::shared_ptr<OptimizerDPBase > &>())
    .def("oneStep", &TransitionStepRegressionDPWrap::oneStepWrap)
    .def("dumpContinuationValues", & TransitionStepRegressionDPWrap::dumpContinuationValuesWrap)
    ;

    // maps SimulateStepRegression for simulation purpose
    class_<SimulateStepRegressionWrap, boost::noncopyable>("SimulateStepRegression", init< gs::BinaryFileArchive &,  const int &,  const std::string &,
            const   boost::shared_ptr< SpaceGrid > & , const  boost::shared_ptr< OptimizerDPBase > & >())
    .def("oneStep", &SimulateStepRegressionWrap::oneStepWrap)
    ;
    class_<SimulateStepRegressionControlWrap, boost::noncopyable>("SimulateStepRegressionControl", init< gs::BinaryFileArchive &,  const int &,  const std::string &,
            const   boost::shared_ptr< SpaceGrid > & , const  boost::shared_ptr< OptimizerDPBase > & >())
    .def("oneStep", &SimulateStepRegressionControlWrap::oneStepWrap)
    ;

#ifdef USE_MPI

    class_<FinalStepRegressionDPDistWrap, boost::noncopyable >("FinalStepRegressionDPDist" , init< const boost::shared_ptr<SpaceGrid>  &, const int &,
            const Eigen::Array< bool, Eigen::Dynamic, 1>  &>())
    .def("set", &FinalStepRegressionDPDistWrap::set)
    .def("_setItem_", &FinalStepRegressionDPDistWrap::operator())
    ;

    class_<TransitionStepRegressionDPDistWrap, boost::noncopyable>("TransitionStepRegressionDPDist", init< const boost::shared_ptr<SpaceGrid> &,
            const  boost::shared_ptr<SpaceGrid> & , const boost::shared_ptr<OptimizerDPBase > &>())
    .def("oneStep", &TransitionStepRegressionDPDistWrap::oneStepWrap)
    .def("dumpContinuationValues", & TransitionStepRegressionDPDistWrap::dumpContinuationValuesWrap)
    ;

    class_<SimulateStepRegressionDistWrap, boost::noncopyable>("SimulateStepRegressionDist", init< gs::BinaryFileArchive &,  const int &,  const std::string &,
            const   boost::shared_ptr< SpaceGrid > & , const  boost::shared_ptr< OptimizerDPBase > &,
            const bool & >())
    .def("oneStep", &SimulateStepRegressionDistWrap::oneStepWrap)
    ;

    class_<SimulateStepRegressionControlDistWrap, boost::noncopyable>("SimulateStepRegressionControlDist", init< gs::BinaryFileArchive &,  const int &,  const std::string &,
            const   boost::shared_ptr< SpaceGrid > & , const   boost::shared_ptr< SpaceGrid > & , const  boost::shared_ptr< OptimizerDPBase > &,
            const bool & >())
    .def("oneStep", &SimulateStepRegressionControlDistWrap::oneStepWrap)
    ;

    def("reconstructProc0Mpi", &reconstructProc0MpiWrap)
    ;


#endif
}
