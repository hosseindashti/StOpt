// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)

/** \file BoostPythonGrids.cpp
 * \brief Map grids  classes to python
 * \author Xavier Warin
 */
#include <Eigen/Dense>
#include <iostream>
#include <memory>
#include <boost/bind.hpp>
#include <boost/version.hpp>
#include "geners/BinaryFileArchive.hh"
#include "StOpt/core/utils/constant.h"
#include "StOpt/core/utils/version.h"
#include "StOpt/core/grids/Interpolator.h"
#include "StOpt/core/grids/LinearInterpolator.h"
#include "StOpt/core/grids/LegendreInterpolator.h"
#include "StOpt/core/grids/SparseNoBoundInterpolator.h"
#include "StOpt/core/grids/SparseBoundInterpolator.h"
#include "StOpt/core/grids/GridIterator.h"
#include "StOpt/core/grids/FullGridIterator.h"
#include "StOpt/core/grids/FullRegularGridIterator.h"
#include "StOpt/core/grids/FullLegendreGridIterator.h"
#include "StOpt/core/grids/FullGeneralGridIterator.h"
#include "StOpt/core/grids/SparseGridNoBoundIterator.h"
#include "StOpt/core/grids/SparseGridBoundIterator.h"
#include "StOpt/core/grids/RegularSpaceGrid.h"
#include "StOpt/core/grids/RegularLegendreGrid.h"
#include "StOpt/core/grids/SparseSpaceGridBound.h"
#include "StOpt/core/grids/SparseSpaceGridNoBound.h"
#include "StOpt/core/grids/OneDimRegularSpaceGrid.h"
#include "StOpt/core/grids/OneDimSpaceGrid.h"
#include "StOpt/core/sparse/sparseGridUtils.h"

using namespace Eigen;
using namespace std;
using namespace StOpt;
using namespace gs;

#ifdef __linux__
#ifdef __clang__
#if BOOST_VERSION <  105600
// map std::shared ptr to boost python
namespace boost
{
template<class T> T *get_pointer(std::shared_ptr<T> p)
{
    return p.get();
}
}
#endif
#endif
#endif

#ifdef _DEBUG
#undef _DEBUG
#include <Python.h>
#define _DEBUG
#else
#include <Python.h>
#endif
#include <numpy/arrayobject.h>
#include "StOpt/python/NumpyConverter.hpp"
#include "StOpt/python/PairConverter.h"

using namespace boost::python;

//  wrapper for iterator on grids
//*******************************
struct  GridIteratorWrap : GridIterator, wrapper<GridIterator>
{
    inline Eigen::ArrayXd  getCoordinate() const
    {
        return	this->get_override("getCoordinate")();
    }
    inline  bool isValid() const
    {
        return	this->get_override("isValid")();
    }
    void next()
    {
        this->get_override("next")();
    }
    void nextInc(const int &p_incr)
    {
        this->get_override("nextInc")(p_incr);
    }
    inline int getCount() const
    {
        return this->get_override("getCount")();
    }
    void jumpToAndInc(const int &p_rank, const int &p_nbProc , const int &p_jump)
    {
        this->get_override("jumpToAndInc")(p_rank, p_nbProc , p_jump);
    }

    int  getRelativePosition()  const
    {
        return this->get_override("getRelativePosition")();
    }

    int getNbPointRelative() const
    {
        return this->get_override("getNbPointRelative")();
    }
    void reset()
    {
        this->get_override("reset")();
    }

};

// wrapper for general grids
struct  FullGeneralGridIteratorWrap : FullGeneralGridIterator, wrapper<FullGeneralGridIterator>
{

    inline  Eigen::ArrayXd  getCoordinate() const
    {
        if (override py_override = this->get_override("getCoordinate"))
        {
            return	this->get_override("getCoordinate")();
        }
        return FullGeneralGridIterator::getCoordinate();
    }
    inline  Eigen::ArrayXd defaultGetCoordinate() const
    {
        return FullGeneralGridIterator::getCoordinate();
    }
    inline  bool isValid() const
    {
        if (override py_override = this->get_override("isValid"))
        {
            return	this->get_override("isValid")();
        }
        return FullGeneralGridIterator::isValid();
    }
    inline bool defaultIsValid() const
    {
        return FullGeneralGridIterator::isValid();
    }
    void next()
    {
        if (override py_override = this->get_override("next"))
        {
            this->get_override("next")();
        }
        FullGeneralGridIterator::next();
    }
    inline void defaultNext()
    {
        FullGeneralGridIterator::next();
    }

    void nextInc(const int &p_incr)
    {
        if (override py_override = this->get_override("nextInc"))
        {
            this->get_override("nextInc")(p_incr);
        }
        FullGeneralGridIterator::nextInc(p_incr);
    }
    inline void  defaultNextInc(const int &p_incr)
    {
        FullGeneralGridIterator::nextInc(p_incr);
    }
    inline int getCount() const
    {
        if (override py_override = this->get_override("getCount"))
        {
            return this->get_override("getCount")();
        }
        return FullGeneralGridIterator::getCount();
    }
    inline int defaultGetCount() const
    {
        return FullGeneralGridIterator::getCount();
    }
    inline void reset()
    {
        return FullGeneralGridIterator::reset();
    }
};

// wrapper for regular grids
struct  FullRegularGridIteratorWrap : FullRegularGridIterator, wrapper<FullRegularGridIterator>
{

    inline  Eigen::ArrayXd  getCoordinate() const
    {
        if (override py_override = this->get_override("getCoordinate"))
        {
            return	this->get_override("getCoordinate")();
        }
        return FullRegularGridIterator::getCoordinate();
    }
    inline  Eigen::ArrayXd defaultGetCoordinate() const
    {
        return FullRegularGridIterator::getCoordinate();
    }
    inline  bool isValid() const
    {
        if (override py_override = this->get_override("isValid"))
        {
            return	this->get_override("isValid")();
        }
        return FullRegularGridIterator::isValid();
    }
    inline bool defaultIsValid() const
    {
        return FullRegularGridIterator::isValid();
    }
    void next()
    {
        if (override py_override = this->get_override("next"))
        {
            this->get_override("next")();
        }
        FullRegularGridIterator::next();
    }
    inline void defaultNext()
    {
        FullRegularGridIterator::next();
    }

    void nextInc(const int &p_incr)
    {
        if (override py_override = this->get_override("nextInc"))
        {
            this->get_override("nextInc")(p_incr);
        }
        FullRegularGridIterator::nextInc(p_incr);
    }
    inline void  defaultNextInc(const int &p_incr)
    {
        FullRegularGridIterator::nextInc(p_incr);
    }
    inline int getCount() const
    {
        if (override py_override = this->get_override("getCount"))
        {
            return this->get_override("getCount")();
        }
        return FullRegularGridIterator::getCount();
    }
    inline int defaultGetCount() const
    {
        return FullRegularGridIterator::getCount();
    }
    inline void reset()
    {
        return FullRegularGridIterator::reset();
    }
};

// wrapper for Legendre grids
struct  FullLegendreGridIteratorWrap : FullLegendreGridIterator, wrapper<FullLegendreGridIterator>
{

    inline  Eigen::ArrayXd  getCoordinate() const
    {
        if (override py_override = this->get_override("getCoordinate"))
        {
            return	this->get_override("getCoordinate")();
        }
        return FullLegendreGridIterator::getCoordinate();
    }
    inline  Eigen::ArrayXd defaultGetCoordinate() const
    {
        return FullLegendreGridIterator::getCoordinate();
    }
    inline  bool isValid() const
    {
        if (override py_override = this->get_override("isValid"))
        {
            return	this->get_override("isValid")();
        }
        return FullLegendreGridIterator::isValid();
    }
    inline bool defaultIsValid() const
    {
        return FullLegendreGridIterator::isValid();
    }
    void next()
    {
        if (override py_override = this->get_override("next"))
        {
            this->get_override("next")();
        }
        FullLegendreGridIterator::next();
    }
    inline void defaultNext()
    {
        FullLegendreGridIterator::next();
    }

    void nextInc(const int &p_incr)
    {
        if (override py_override = this->get_override("nextInc"))
        {
            this->get_override("nextInc")(p_incr);
        }
        FullLegendreGridIterator::nextInc(p_incr);
    }
    inline void  defaultNextInc(const int &p_incr)
    {
        FullLegendreGridIterator::nextInc(p_incr);
    }
    inline int getCount() const
    {
        if (override py_override = this->get_override("getCount"))
        {
            return this->get_override("getCount")();
        }
        return FullLegendreGridIterator::getCount();
    }
    inline int defaultGetCount() const
    {
        return FullLegendreGridIterator::getCount();
    }
    inline void reset()
    {
        return FullLegendreGridIterator::reset();
    }
};

// wrapper for Sparse grids with out  bounds
struct  SparseGridNoBoundIteratorWrap : SparseGridNoBoundIterator, wrapper<SparseGridNoBoundIterator>
{

    inline  Eigen::ArrayXd  getCoordinate() const
    {
        if (override py_override = this->get_override("getCoordinate"))
        {
            return	this->get_override("getCoordinate")();
        }
        return SparseGridNoBoundIterator::getCoordinate();
    }
    inline  Eigen::ArrayXd defaultGetCoordinate() const
    {
        return SparseGridNoBoundIterator::getCoordinate();
    }
    inline  bool isValid() const
    {
        if (override py_override = this->get_override("isValid"))
        {
            return	this->get_override("isValid")();
        }
        return SparseGridNoBoundIterator::isValid();
    }
    inline bool defaultIsValid() const
    {
        return SparseGridNoBoundIterator::isValid();
    }
    void next()
    {
        if (override py_override = this->get_override("next"))
        {
            this->get_override("next")();
        }
        SparseGridNoBoundIterator::next();
    }
    inline void defaultNext()
    {
        SparseGridNoBoundIterator::next();
    }

    void nextInc(const int &p_incr)
    {
        if (override py_override = this->get_override("nextInc"))
        {
            this->get_override("nextInc")(p_incr);
        }
        SparseGridNoBoundIterator::nextInc(p_incr);
    }
    inline void  defaultNextInc(const int &p_incr)
    {
        SparseGridNoBoundIterator::nextInc(p_incr);
    }
    inline int getCount() const
    {
        if (override py_override = this->get_override("getCount"))
        {
            return this->get_override("getCount")();
        }
        return SparseGridNoBoundIterator::getCount();
    }
    inline int defaultGetCount() const
    {
        return SparseGridNoBoundIterator::getCount();
    }
    inline void reset()
    {
        return SparseGridNoBoundIterator::reset();
    }
};


// wrapper for Sparse grids with  bounds
struct  SparseGridBoundIteratorWrap : SparseGridBoundIterator, wrapper<SparseGridBoundIterator>
{

    inline  Eigen::ArrayXd  getCoordinate() const
    {
        if (override py_override = this->get_override("getCoordinate"))
        {
            return	this->get_override("getCoordinate")();
        }
        return SparseGridBoundIterator::getCoordinate();
    }
    inline  Eigen::ArrayXd defaultGetCoordinate() const
    {
        return SparseGridBoundIterator::getCoordinate();
    }
    inline  bool isValid() const
    {
        if (override py_override = this->get_override("isValid"))
        {
            return	this->get_override("isValid")();
        }
        return SparseGridBoundIterator::isValid();
    }
    inline bool defaultIsValid() const
    {
        return SparseGridBoundIterator::isValid();
    }
    void next()
    {
        if (override py_override = this->get_override("next"))
        {
            this->get_override("next")();
        }
        SparseGridBoundIterator::next();
    }
    inline void defaultNext()
    {
        SparseGridBoundIterator::next();
    }

    void nextInc(const int &p_incr)
    {
        if (override py_override = this->get_override("nextInc"))
        {
            this->get_override("nextInc")(p_incr);
        }
        SparseGridBoundIterator::nextInc(p_incr);
    }
    inline void  defaultNextInc(const int &p_incr)
    {
        SparseGridBoundIterator::nextInc(p_incr);
    }
    inline int getCount() const
    {
        if (override py_override = this->get_override("getCount"))
        {
            return this->get_override("getCount")();
        }
        return SparseGridBoundIterator::getCount();
    }
    inline int defaultGetCount() const
    {
        return SparseGridBoundIterator::getCount();
    }
    inline void reset()
    {
        return SparseGridBoundIterator::reset();
    }
};


// wrapper for interpolator
struct  InterpolatorWrap : Interpolator, wrapper< Interpolator>
{
    inline double apply(const Eigen::ArrayXd &p_dataValues) const
    {
        return	this->get_override("apply")(p_dataValues);
    }
    inline Eigen::ArrayXd applyVec(const Eigen::ArrayXXd &p_dataValues) const
    {
        return 	this->get_override("applyVec")(p_dataValues);
    }
};

// wrapper for linear interpolator
struct LinearInterpolatorWrap : LinearInterpolator, wrapper<LinearInterpolator>
{
    inline double apply(const Eigen::ArrayXd &p_dataValues) const
    {
        if (override py_override = this->get_override("apply"))
        {
            return	this->get_override("apply")(p_dataValues);
        }
        return LinearInterpolator::apply(p_dataValues);
    }

    inline Eigen::ArrayXd applyVec(const Eigen::ArrayXXd &p_dataValues) const
    {
        if (override py_override = this->get_override("applyVec"))
        {
            return	this->get_override("applyVec")(p_dataValues);
        }
        return LinearInterpolator::applyVec(p_dataValues);
    }
};


// wrapper for legendre interpolator
struct LegendreInterpolatorWrap : LegendreInterpolator, wrapper<LegendreInterpolator>
{
    inline double apply(const Eigen::ArrayXd &p_dataValues) const
    {
        if (override py_override = this->get_override("apply"))
        {
            return	this->get_override("apply")(p_dataValues);
        }
        return LegendreInterpolator::apply(p_dataValues);
    }

    inline Eigen::ArrayXd applyVec(const Eigen::ArrayXXd &p_dataValues) const
    {
        if (override py_override = this->get_override("applyVec"))
        {
            return	this->get_override("applyVec")(p_dataValues);
        }
        return LegendreInterpolator::applyVec(p_dataValues);
    }
};

// wrapper for sparse grid interpolator without boundary points
struct SparseNoBoundInterpolatorLinearWrap : SparseNoBoundInterpolator<LinearHatValue, LinearHatValue , LinearHatValue  >, wrapper<SparseNoBoundInterpolator< LinearHatValue, LinearHatValue , LinearHatValue> >
{
    inline double apply(const Eigen::ArrayXd &p_dataValues) const
    {
        if (override py_override = this->get_override("apply"))
        {
            return	this->get_override("apply")(p_dataValues);
        }
        return SparseNoBoundInterpolator<LinearHatValue, LinearHatValue , LinearHatValue  >::apply(p_dataValues);
    }
    inline Eigen::ArrayXd applyVec(const Eigen::ArrayXXd &p_dataValues) const
    {
        if (override py_override = this->get_override("applyVec"))
        {
            return	this->get_override("applyVec")(p_dataValues);
        }
        return SparseNoBoundInterpolator<LinearHatValue, LinearHatValue , LinearHatValue  >::applyVec(p_dataValues);
    }
};


// wrapper for sparse grid interpolator without boundary points
struct SparseNoBoundInterpolatorQuadraticWrap : SparseNoBoundInterpolator<QuadraticValue, QuadraticValue , QuadraticValue  >, wrapper<SparseNoBoundInterpolator< QuadraticValue, QuadraticValue , QuadraticValue> >
{
    inline double apply(const Eigen::ArrayXd &p_dataValues) const
    {
        if (override py_override = this->get_override("apply"))
        {
            return	this->get_override("apply")(p_dataValues);
        }
        return SparseNoBoundInterpolator<QuadraticValue, QuadraticValue , QuadraticValue  >::apply(p_dataValues);
    }
    inline Eigen::ArrayXd applyVec(const Eigen::ArrayXXd &p_dataValues) const
    {
        if (override py_override = this->get_override("applyVec"))
        {
            return	this->get_override("applyVec")(p_dataValues);
        }
        return SparseNoBoundInterpolator<QuadraticValue, QuadraticValue , QuadraticValue  >::applyVec(p_dataValues);
    }

};

// wrapper for sparse grid interpolator with boundary points
struct SparseBoundInterpolatorLinearWrap : SparseBoundInterpolator<LinearHatValue, LinearHatValue , LinearHatValue  >, wrapper<SparseBoundInterpolator< LinearHatValue, LinearHatValue , LinearHatValue> >
{
    inline double apply(const Eigen::ArrayXd &p_dataValues) const
    {
        if (override py_override = this->get_override("apply"))
        {
            return	this->get_override("apply")(p_dataValues);
        }
        return SparseBoundInterpolator<LinearHatValue, LinearHatValue , LinearHatValue  >::apply(p_dataValues);
    }
    inline Eigen::ArrayXd applyVec(const Eigen::ArrayXXd &p_dataValues) const
    {
        if (override py_override = this->get_override("applyVec"))
        {
            return	this->get_override("applyVec")(p_dataValues);
        }
        return SparseBoundInterpolator<LinearHatValue, LinearHatValue , LinearHatValue  >::applyVec(p_dataValues);
    }
};


// wrapper for sparse grid interpolator with boundary points
struct SparseBoundInterpolatorQuadraticWrap : SparseBoundInterpolator<QuadraticValue, QuadraticValue , QuadraticValue  >, wrapper<SparseBoundInterpolator< QuadraticValue, QuadraticValue , QuadraticValue> >
{
    inline double apply(const Eigen::ArrayXd &p_dataValues) const
    {
        if (override py_override = this->get_override("apply"))
        {
            return	this->get_override("apply")(p_dataValues);
        }
        return SparseBoundInterpolator<QuadraticValue, QuadraticValue , QuadraticValue  >::apply(p_dataValues);
    }
    inline Eigen::ArrayXd applyVec(const Eigen::ArrayXXd &p_dataValues) const
    {
        if (override py_override = this->get_override("applyVec"))
        {
            return	this->get_override("applyVec")(p_dataValues);
        }
        return SparseBoundInterpolator<QuadraticValue, QuadraticValue , QuadraticValue  >::applyVec(p_dataValues);
    }
};


// wrapper for grids
struct  SpaceGridWrap : SpaceGrid, wrapper< SpaceGrid>
{
    inline size_t getNbPoints() const
    {
        return	this->get_override("getNbPoints")();
    }

    shared_ptr<GridIterator> getGridIterator() const
    {
        return this->get_override("getGridIterator")();
    }
    shared_ptr<GridIterator> getGridIteratorInc(const int &p_iThread) const
    {
        return this->get_override("getGridIteratorInc")(p_iThread);
    }
    shared_ptr<Interpolator> createInterpolator(const Eigen::ArrayXd &p_coord) const
    {
        return this->get_override("createInterpolator")(p_coord);
    }
    shared_ptr<InterpolatorSpectral> createInterpolatorSpectral(const Eigen::ArrayXd &p_values) const
    {
        return this->get_override("createInterpolatorSpectral")(p_values);
    }
    inline int getDimension() const
    {
        return this->get_override("getDimension")();
    }
    inline std::vector <std::array< double, 2>  > getExtremeValues() const
    {
        return  this->get_override("getExtremeValues")();
    }
    inline bool isStrictlyInside(const Eigen::ArrayXd &p_point) const
    {
        return  this->get_override("isStrictlyInside")(p_point);
    }
    inline bool isInside(const Eigen::ArrayXd &p_point) const
    {
        return  this->get_override("isInside")(p_point);
    }
    void truncatePoint(Eigen::ArrayXd &p_point) const
    {
        this->get_override("truncatepoint")(p_point);
    }

};

struct VecArray2ToList
{
    static PyObject *convert(const std::vector<  std::array<double, 2> > &p_vec)
    {
        boost::python::list *l = new boost::python::list();
        for (size_t i = 0; i < p_vec.size(); i++)
        {
            boost::python::list *lInside = new boost::python::list();
            (*lInside).append(p_vec[i][0]);
            (*lInside).append(p_vec[i][1]);
            (*l).append(*lInside);
        }
        return l->ptr();
    }
};

template< class Sparse>
std::pair<ArrayXd, ArrayXd>  refineSparseWrapp(Sparse &p_sparse, const double &p_precision, object &p_fInterpol,
        const Eigen::ArrayXd &p_valuesFunction,
        const Eigen::ArrayXd &p_hierarValues)
{
    auto phiLam([](const SparseSet::const_iterator & p_iterLevel, const Eigen::ArrayXd & p_values)
    {
        double smax = -infty;
        for (const auto &index : p_iterLevel->second)
            smax = max(smax, fabs(p_values(index.second)));
        return smax;
    });
    std::function< double(const SparseSet::const_iterator &, const Eigen::ArrayXd &)> phi(std::cref(phiLam));
    auto phiMultLam([](const vector< double> &p_vec)
    {
        assert(p_vec.size() > 0);
        double smax = p_vec[0];
        for (size_t i = 1; i < p_vec.size(); ++i)
            smax = max(smax, p_vec[i]);
        return smax;
    });
    std::function< double(const std::vector< double> &) > phiMult(std::cref(phiMultLam));

    Eigen::ArrayXd  valuesFunction(p_valuesFunction);
    Eigen::ArrayXd  hierarValues(p_hierarValues);
    auto lambda = [p_fInterpol](const Eigen::ArrayXd & p_x)->double { return boost::python::extract<double>(p_fInterpol(p_x));};

    p_sparse.refine(p_precision, std::function<double(const Eigen::ArrayXd &p_x)> (lambda), phi, phiMult, valuesFunction, hierarValues);
    return make_pair(valuesFunction, hierarValues);
}


template< class Sparse>
std::pair<ArrayXd, ArrayXd>  coarsenSparseWrapp(Sparse &p_sparse, const double &p_precision, const Eigen::ArrayXd &p_valuesFunction,
        const Eigen::ArrayXd   &p_hierarValues)
{
    auto phiLam([](const SparseSet::const_iterator & p_iterLevel, const Eigen::ArrayXd & p_values)
    {
        double smax = -infty;
        for (SparseLevel::const_iterator iterIndex = p_iterLevel->second.begin(); iterIndex != p_iterLevel->second.end(); ++iterIndex)
        {
            smax = max(smax, fabs(p_values(iterIndex->second)));
        }
        return smax;
    });
    std::function< double(const SparseSet::const_iterator &, const Eigen::ArrayXd &)> phi(std::cref(phiLam));

    Eigen::ArrayXd  valuesFunction(p_valuesFunction);
    Eigen::ArrayXd  hierarValues(p_hierarValues);
    p_sparse.coarsen(p_precision, phi, valuesFunction, hierarValues);

    return make_pair(valuesFunction, hierarValues);
}

/// \brief Encapsulation for regression module
BOOST_PYTHON_MODULE(StOptGrids)
{

    Register<ArrayXXd>();
    Register<ArrayXd>();
    Register<ArrayXi>();


    boost::python::type_info infoVecToList = boost::python::type_id< std::vector< std::array< double, 2> , std::allocator< std::array< double, 2> > > >();
    const boost::python::converter::registration *reg = boost::python::converter::registry::query(infoVecToList);
    // check if already registered
    if (reg == NULL)
    {
        to_python_converter<  std::vector< std::array< double, 2> , std::allocator< std::array< double, 2> > >, VecArray2ToList >();
    }
    else if ((*reg).m_to_python == NULL)
    {
        to_python_converter<  std::vector< std::array< double, 2> , std::allocator< std::array< double, 2> > > , VecArray2ToList >();
    }
    boost::python::type_info info = boost::python::type_id< std::pair<ArrayXd, ArrayXd > > ();
    const boost::python::converter::registration *reg1 = boost::python::converter::registry::query(info);
    // check if already registered
    if (reg1 == NULL)
    {
        to_python_converter<  std::pair<ArrayXd, ArrayXd  > , pairSpec1< ArrayXd, ArrayXd > >() ;
    }
    else if ((*reg1).m_to_python == NULL)
    {
        to_python_converter<  std::pair<ArrayXd, ArrayXd  > , pairSpec1< ArrayXd, ArrayXd >  >() ;
    }


    using namespace boost::python;

    // version
    def("getVersion", getStOptVersion);

    // Map iterators on grids
    //***********************
    class_< GridIteratorWrap, std::shared_ptr<GridIteratorWrap> , boost::noncopyable>("GridIterator")
    .def("getCoordinate", pure_virtual(&GridIterator::getCoordinate))
    .def("isValid", pure_virtual(&GridIterator::isValid))
    .def("next", pure_virtual(&GridIterator::next))
    .def("nextInc", pure_virtual(&GridIterator::nextInc))
    .def("getCount", pure_virtual(&GridIterator::getCount))
    .def("getNbPointRelative", pure_virtual(&GridIterator::getNbPointRelative))
    ;

    // for shared_ptr<GridIterator>.
    objects::class_value_wrapper< std::shared_ptr<GridIterator>, objects::make_ptr_instance<GridIterator, objects::pointer_holder<std::shared_ptr<GridIterator>, GridIterator> > >();

    //  ///  to map the constructor , should map  std::shared_ptr with boost python
    class_<FullGeneralGridIteratorWrap,  std::shared_ptr< FullGeneralGridIteratorWrap>, bases<GridIterator>, boost::noncopyable >("FullGeneralGridIterator")
    .def("getCoordinate", &FullGeneralGridIterator::getCoordinate, &FullGeneralGridIteratorWrap::defaultGetCoordinate)
    .def("isValid", &FullGeneralGridIterator::isValid, &FullGeneralGridIteratorWrap::defaultIsValid)
    .def("next", &FullGeneralGridIterator::next, &FullGeneralGridIteratorWrap::defaultNext)
    .def("nextInc", &FullGeneralGridIterator::nextInc, &FullGeneralGridIteratorWrap::defaultNextInc)
    .def("getCount", &FullGeneralGridIterator::getCount, &FullGeneralGridIteratorWrap::defaultGetCount)
    ;


    class_<FullRegularGridIteratorWrap, std::shared_ptr< FullRegularGridIteratorWrap>, bases<GridIterator>, boost::noncopyable >("FullRegularGridIterator")
    .def("getCoordinate", &FullRegularGridIterator::getCoordinate, &FullRegularGridIteratorWrap::defaultGetCoordinate)
    .def("isValid", &FullRegularGridIterator::isValid, &FullRegularGridIteratorWrap::defaultIsValid)
    .def("next", &FullRegularGridIterator::next, &FullRegularGridIteratorWrap::defaultNext)
    .def("nextInc", &FullRegularGridIterator::nextInc, &FullRegularGridIteratorWrap::defaultNextInc)
    .def("getCount", &FullRegularGridIterator::getCount, &FullRegularGridIteratorWrap::defaultGetCount)
    ;


    class_<FullLegendreGridIteratorWrap, std::shared_ptr<FullLegendreGridIteratorWrap>, bases<GridIterator>, boost::noncopyable >("FullLegendreGridIterator")
    .def("getCoordinate", &FullLegendreGridIterator::getCoordinate, &FullLegendreGridIteratorWrap::defaultGetCoordinate)
    .def("isValid", &FullLegendreGridIterator::isValid, &FullLegendreGridIteratorWrap::defaultIsValid)
    .def("next", &FullLegendreGridIterator::next, &FullLegendreGridIteratorWrap::defaultNext)
    .def("nextInc", &FullLegendreGridIterator::nextInc, &FullLegendreGridIteratorWrap::defaultNextInc)
    .def("getCount", &FullLegendreGridIterator::getCount, &FullLegendreGridIteratorWrap::defaultGetCount)
    ;

    class_<SparseGridNoBoundIteratorWrap, std::shared_ptr< SparseGridNoBoundIteratorWrap>, bases<GridIterator>, boost::noncopyable >("SparseGridNoBoundIterator")
    .def("getCoordinate", &SparseGridNoBoundIterator::getCoordinate, &SparseGridNoBoundIteratorWrap::defaultGetCoordinate)
    .def("isValid", &SparseGridNoBoundIterator::isValid, &SparseGridNoBoundIteratorWrap::defaultIsValid)
    .def("next", &SparseGridNoBoundIterator::next, &SparseGridNoBoundIteratorWrap::defaultNext)
    .def("nextInc", &SparseGridNoBoundIterator::nextInc, &SparseGridNoBoundIteratorWrap::defaultNextInc)
    .def("getCount", &SparseGridNoBoundIterator::getCount, &SparseGridNoBoundIteratorWrap::defaultGetCount)
    ;

    class_<SparseGridBoundIteratorWrap, std::shared_ptr< SparseGridBoundIteratorWrap>, bases<GridIterator>, boost::noncopyable >("SparseGridBoundIterator")
    .def("getCoordinate", &SparseGridBoundIterator::getCoordinate, &SparseGridBoundIteratorWrap::defaultGetCoordinate)
    .def("isValid", &SparseGridBoundIterator::isValid, &SparseGridBoundIteratorWrap::defaultIsValid)
    .def("next", &SparseGridBoundIterator::next, &SparseGridBoundIteratorWrap::defaultNext)
    .def("nextInc", &SparseGridBoundIterator::nextInc, &SparseGridBoundIteratorWrap::defaultNextInc)
    .def("getCount", &SparseGridBoundIterator::getCount, &SparseGridBoundIteratorWrap::defaultGetCount)
    ;

    // Interpolators
    // *************
    class_<InterpolatorWrap,  std::shared_ptr< InterpolatorWrap>, boost::noncopyable>("Interpolator")
    .def("apply", pure_virtual(&Interpolator::apply))
    .def("applyVec", pure_virtual(&Interpolator::applyVec))
    ;

    // for shared_ptr<Interpolator>.
    objects::class_value_wrapper< std::shared_ptr<Interpolator>, objects::make_ptr_instance<Interpolator, objects::pointer_holder<std::shared_ptr<Interpolator>, Interpolator> > >();

    class_<LinearInterpolatorWrap, std::shared_ptr<LinearInterpolatorWrap>, bases<Interpolator > , boost::noncopyable >("LinearInterpolator")
    .def("apply", &LinearInterpolator::apply, &LinearInterpolatorWrap::apply)
    .def("applyVec", &LinearInterpolator::applyVec, &LinearInterpolatorWrap::applyVec)
    ;

    class_<LegendreInterpolatorWrap, std::shared_ptr<LegendreInterpolatorWrap>, bases<Interpolator > , boost::noncopyable >("LegendreInterpolator")
    .def("apply", &LegendreInterpolator::apply, &LegendreInterpolatorWrap::apply)
    .def("applyVec", &LegendreInterpolator::applyVec, &LegendreInterpolatorWrap::applyVec)
    ;


    class_<SparseNoBoundInterpolatorLinearWrap, std::shared_ptr<SparseNoBoundInterpolatorLinearWrap>, bases<Interpolator > , boost::noncopyable >("SparseNoBoundInterpolatorLinear")
    .def("apply", &SparseNoBoundInterpolator<LinearHatValue, LinearHatValue , LinearHatValue  >::apply, &SparseNoBoundInterpolatorLinearWrap::apply)
    .def("applyVec", &SparseNoBoundInterpolator<LinearHatValue, LinearHatValue , LinearHatValue  >::applyVec, &SparseNoBoundInterpolatorLinearWrap::applyVec)
    ;

    class_<SparseNoBoundInterpolatorQuadraticWrap, std::shared_ptr<SparseNoBoundInterpolatorQuadraticWrap>, bases<Interpolator > , boost::noncopyable >("SparseNoBoundInterpolatorQuadratic")
    .def("apply", &SparseNoBoundInterpolator<QuadraticValue, QuadraticValue , QuadraticValue  >::apply, &SparseNoBoundInterpolatorQuadraticWrap::apply)
    .def("applyVec", &SparseNoBoundInterpolator<QuadraticValue, QuadraticValue , QuadraticValue  >::applyVec, &SparseNoBoundInterpolatorQuadraticWrap::applyVec)
    ;

    class_<SparseBoundInterpolatorLinearWrap, std::shared_ptr<SparseBoundInterpolatorLinearWrap>, bases<Interpolator > , boost::noncopyable >("SparseBoundInterpolatorLinear")
    .def("apply", &SparseBoundInterpolator<LinearHatValue, LinearHatValue , LinearHatValue  >::apply, &SparseBoundInterpolatorLinearWrap::apply)
    .def("applyVec", &SparseBoundInterpolator<LinearHatValue, LinearHatValue , LinearHatValue  >::applyVec, &SparseBoundInterpolatorLinearWrap::applyVec)
    ;

    class_<SparseBoundInterpolatorQuadraticWrap, std::shared_ptr<SparseBoundInterpolatorQuadraticWrap>, bases<Interpolator > , boost::noncopyable >("SparseBoundInterpolatorQuadratic")
    .def("apply", &SparseBoundInterpolator<QuadraticValue, QuadraticValue , QuadraticValue  >::apply, &SparseBoundInterpolatorQuadraticWrap::apply)
    .def("applyVec", &SparseBoundInterpolator<QuadraticValue, QuadraticValue , QuadraticValue  >::applyVec, &SparseBoundInterpolatorQuadraticWrap::applyVec)
    ;

    // Map grids
    //**********
    class_<SpaceGridWrap, std::shared_ptr<SpaceGridWrap>, boost::noncopyable>("SpaceGrid")
    .def("getNbPoints", &SpaceGrid::getNbPoints)
    .def("getGridIterator", &SpaceGrid::getGridIterator)
    .def("getGridIteratorInc", &SpaceGrid::getGridIteratorInc)
    .def("createInterpolator", &SpaceGrid::createInterpolator)
    .def("isStrictlyInside", &SpaceGrid::isStrictlyInside)
    .def("isInside", &SpaceGrid::isInside)
    .def("getDimension", &SpaceGrid::getDimension)
    .def("getExtremeValues", &SpaceGrid::getExtremeValues)
    ;

    class_<RegularSpaceGrid , std::shared_ptr<RegularSpaceGrid>, bases<SpaceGrid> >("RegularSpaceGrid", init<   const Eigen::ArrayXd &, const Eigen::ArrayXd &, const  Eigen::ArrayXi & >())
    .def("lowerPositionCoord", &RegularSpaceGrid::lowerPositionCoord)
    .def("upperPositionCoord", &RegularSpaceGrid::upperPositionCoord)
    .def("getMeshSize", &RegularSpaceGrid::getMeshSize)
    .def("getDimensions", &RegularSpaceGrid::getDimensions, return_internal_reference<>())

    ;

    class_<RegularLegendreGrid, std::shared_ptr<RegularLegendreGrid>, bases<SpaceGrid> >("RegularLegendreGrid", init< const Eigen::ArrayXd &, const Eigen::ArrayXd , const  Eigen::ArrayXi &, const Eigen::ArrayXi & >())
    .def(init< >())
    .def("lowerPositionCoord", &RegularSpaceGrid::lowerPositionCoord)
    .def("upperPositionCoord", &RegularSpaceGrid::upperPositionCoord)
    .def("getMeshSize", &RegularSpaceGrid::getMeshSize)
    .def("getDimensions", &RegularSpaceGrid::getDimensions, return_internal_reference<>())
    ;


    class_<SparseSpaceGridBound, std::shared_ptr<SparseSpaceGridBound>, bases<SpaceGrid> >("SparseSpaceGridBound")
    .def(init< const int &,  const Eigen::ArrayXd &, const size_t &> ())
    .def(init< const Eigen::ArrayXd &, const Eigen::ArrayXd &, const int &,  const Eigen::ArrayXd &, const size_t & > ())
    .def("toHierarchize", &SparseSpaceGridBound::toHierarchizeD)
    .def("toHierarchizeVec", &SparseSpaceGridBound::toHierarchizeVecD)
    .def("refine", &refineSparseWrapp<SparseSpaceGridBound>)
    .def("coarsen", &coarsenSparseWrapp<SparseSpaceGridBound>)
    ;

    class_<SparseSpaceGridNoBound, std::shared_ptr<SparseSpaceGridNoBound>, bases<SpaceGrid> >("SparseSpaceGridNoBound")
    .def(init< const int &,  const Eigen::ArrayXd &, const size_t &> ())
    .def(init< const Eigen::ArrayXd &, const Eigen::ArrayXd &, const int &,  const Eigen::ArrayXd &, const size_t & > ())
    .def("toHierarchize", &SparseSpaceGridNoBound::toHierarchizeD)
    .def("toHierarchizeVec", &SparseSpaceGridNoBound::toHierarchizeVecD)
    .def("refine", &refineSparseWrapp<SparseSpaceGridNoBound>)
    .def("coarsen", &coarsenSparseWrapp<SparseSpaceGridNoBound>)
    ;

    register_ptr_to_python< std::shared_ptr<SpaceGrid > >();

    class_<OneDimRegularSpaceGrid, std::shared_ptr<OneDimRegularSpaceGrid> >("OneDimRegularSpaceGrid", init< const double &, const double &, const  int & >())
    .def("getMesh", &OneDimRegularSpaceGrid::getMesh)
    ;

    class_<OneDimSpaceGrid, std::shared_ptr<OneDimSpaceGrid>>("OneDimSpaceGrid", init<const Eigen::ArrayXd &>())
            .def("getMesh", &OneDimSpaceGrid::getMesh)
            .def("getNbStep", &OneDimSpaceGrid::getNbStep)
            ;
}
