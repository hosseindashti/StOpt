// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)

/** \file BoostPythonGrids.cpp
 * \brief Map grids  classes to python
 * \author Xavier Warin
 */
#include <Eigen/Dense>
#include <iostream>
#include <memory>
#include <boost/bind.hpp>
#include <boost/version.hpp>
#include "geners/BinaryFileArchive.hh"
#include "StOpt/core/grids/SpaceGrid.h"
#include "StOpt/core/utils/Polynomials1D.h"
#include "StOpt/regression/LocalLinearRegression.h"
#include "StOpt/regression/LocalConstRegression.h"
#include "StOpt/regression/LocalSameSizeLinearRegression.h"
#include "StOpt/regression/LocalSameSizeConstRegression.h"
#include "StOpt/regression/SparseRegression.h"
#include "StOpt/regression/GlobalRegression.h"
#include "StOpt/regression/ContinuationValue.h"
#include "StOpt/regression/GridAndRegressedValue.h"

using namespace Eigen;
using namespace std;
using namespace StOpt;
using namespace gs;

#ifdef __linux__
#ifdef __clang__
#if BOOST_VERSION <  105600
// map std::shared ptr to boost python
namespace boost
{
template<class T> T *get_pointer(std::shared_ptr<T> p)
{
    return p.get();
}
}
#endif
#endif
#endif

template<typename T>
void do_release(typename boost::shared_ptr<T> const &, T *)
{
}

template<typename T>
typename std::shared_ptr<T> make_shared_ptr(typename boost::shared_ptr<T> const &p)
{
    return
        std::shared_ptr<T>(
            p.get(),
            boost::bind(&do_release<T>, p, _1));

}

#ifdef _DEBUG
#undef _DEBUG
#include <Python.h>
#define _DEBUG
#else
#include <Python.h>
#endif
#include <numpy/arrayobject.h>
#include "StOpt/python/NumpyConverter.hpp"

using namespace boost::python;


// Wrapper for regression methods
//******************************


/// wrapper for BaseRegression
struct  BaseRegressionWrap : BaseRegression, wrapper<BaseRegression>
{
    void updateSimulations(const bool &p_bZeroDate, const shared_ptr< Eigen::ArrayXXd> &p_particles)
    {
        this->get_override("updateSimulations")(p_bZeroDate, p_particles);
    }

    Eigen::ArrayXd getCoordBasisFunction(const Eigen::ArrayXd &p_fToRegress) const
    {
        return this->get_override("getCoordBasisFunction")(p_fToRegress)   ;
    }

    Eigen::ArrayXXd getCoordBasisFunctionMultiple(const Eigen::ArrayXXd &p_fToRegress) const
    {
        return this->get_override("getCoordBasisFunctionMultiple")(p_fToRegress);
    }

    double reconstructionASim(const int &p_isim , const Eigen::ArrayXd   &p_basisCoefficients) const
    {
        return this->get_override("reconstructionASim")(p_isim , p_basisCoefficients);
    }

    Eigen::ArrayXd getAllSimulations(const Eigen::ArrayXd &p_fToRegress) const
    {
        return this->get_override("getAllSimulations")(p_fToRegress);
    }

    Eigen::ArrayXXd getAllSimulationsMultiple(const Eigen::ArrayXXd &p_fToRegress)  const
    {
        return this->get_override("getAllSimulationsMultiple")(p_fToRegress);
    }
    Eigen::ArrayXd reconstruction(const Eigen::ArrayXd   &p_basisCoefficients) const
    {
        return this->get_override("reconstruction")(p_basisCoefficients);
    }
    Eigen::ArrayXXd reconstructionMultiple(const Eigen::ArrayXXd   &p_basisCoefficients) const
    {
        return this->get_override("reconstructionMultiple")(p_basisCoefficients);
    }
    double getValue(const Eigen::ArrayXd   &p_coordinates, const Eigen::ArrayXd   &p_coordBasisFunction) const
    {
        return this->get_override("getValue")(p_coordinates, p_coordBasisFunction);
    }
    double getAValue(const Eigen::ArrayXd   &p_coordinates, const Eigen::ArrayXd &p_ptOfStock,
                     const std::vector< std::shared_ptr<InterpolatorSpectral> > &m_interpFuncBasis) const
    {
        return this->get_override("getAValue")(p_coordinates, p_ptOfStock, m_interpFuncBasis);
    }
    Eigen::ArrayXXd getParticlesRef()
    {
        return this->get_override("getParticle")();
    }
    int getNumberOfFunction() const
    {
        return this->get_override("getNumberOfFunction")();
    }
    std::shared_ptr<BaseRegression> clone() const
    {
        return this->get_override("clone")();
    }

};

// wrapper for continuation value : trick use boost::shared_ptr and convert to std::shared_ptr
// unable to wrap correctly so copy
class ContinuationValueWrap: ContinuationValue
{
public :
    ContinuationValueWrap(const boost::shared_ptr< SpaceGrid >   &p_grid ,  const boost::shared_ptr<BaseRegression>   &p_espCond,    const Eigen::ArrayXXd &p_values):
        ContinuationValue(make_shared_ptr<SpaceGrid>(p_grid), make_shared_ptr<BaseRegression>(p_espCond), p_values) {  }


    explicit ContinuationValueWrap(const boost::shared_ptr<BaseRegression>   &p_espCond): ContinuationValue(make_shared_ptr<BaseRegression>(p_espCond)) {}

    void loadForSimulation(const  shared_ptr< SpaceGrid > &p_grid , const shared_ptr< BaseRegression >   &p_condExp, const Eigen::ArrayXXd &p_values)
    {
        ContinuationValue::loadForSimulation(p_grid, p_condExp, p_values);
    }
    void loadCondExpForSimulation(const shared_ptr< BaseRegression>   &p_condExp)
    {
        ContinuationValue::loadCondExpForSimulation(p_condExp);
    }
    Eigen::ArrayXd getAllSimulations(const Eigen::ArrayXd &p_ptOfStock)
    {
        return ContinuationValue::getAllSimulations(p_ptOfStock);
    }
    Eigen::ArrayXd getAllSimulationsInterp(const Interpolator   &p_interpol) const
    {
        return ContinuationValue::getAllSimulations(p_interpol);
    }
    double  getASimulation(const int &p_isim, const Interpolator   &p_interpol) const
    {
        return ContinuationValue::getASimulation(p_isim, p_interpol);
    }
    double getValue(const Eigen::ArrayXd &p_ptOfStock, const Eigen::ArrayXd &p_coordinates) const
    {
        return ContinuationValue::getValue(p_ptOfStock, p_coordinates);
    }
    shared_ptr< SpaceGrid > getGrid() const
    {
        return  ContinuationValue::getGrid();
    }
    shared_ptr<BaseRegression > getCondExp()const
    {
        return ContinuationValue::getCondExp();
    }
    int getNbSimul() const
    {
        return ContinuationValue::getNbSimul();
    }
    inline Eigen::ArrayXXd  getParticles() const
    {
        return *ContinuationValue::getParticles();
    }
};


// wrapper for continuation value : trick use boost::shared_ptr and convert to std::shared_ptr
// unable to wrap correctly so copy
class GridAndRegressedValueWrap: GridAndRegressedValue
{
public :
    GridAndRegressedValueWrap(const boost::shared_ptr< SpaceGrid >   &p_grid ,  const boost::shared_ptr<BaseRegression>   &p_regressor  ,  const Eigen::ArrayXXd &p_values):
        GridAndRegressedValue(make_shared_ptr<SpaceGrid>(p_grid), make_shared_ptr<BaseRegression>(p_regressor), p_values) {  }

    GridAndRegressedValueWrap(const boost::shared_ptr< SpaceGrid >   &p_grid ,  const boost::shared_ptr<BaseRegression>   &p_regressor):
        GridAndRegressedValue(make_shared_ptr<SpaceGrid>(p_grid), make_shared_ptr<BaseRegression>(p_regressor)) {  }


    explicit GridAndRegressedValueWrap(const boost::shared_ptr<BaseRegression>   &p_regressor): GridAndRegressedValue(make_shared_ptr<BaseRegression>(p_regressor)) {}


    double getValue(const Eigen::ArrayXd &p_ptOfStock, const Eigen::ArrayXd &p_coordinates) const
    {
        return GridAndRegressedValue::getValue(p_ptOfStock, p_coordinates);
    }

    ArrayXXd  getRegressedValues() const
    {
        return GridAndRegressedValue::getRegressedValues();
    }

    void setRegressedValues(const Eigen::ArrayXXd &p_regValues)
    {
        GridAndRegressedValue::setRegressedValues(p_regValues);
    }

    shared_ptr< SpaceGrid > getGrid() const
    {
        return  GridAndRegressedValue::getGrid();
    }
    shared_ptr<BaseRegression > getRegressor()const
    {
        return GridAndRegressedValue::getRegressor();
    }
};


/// \brief Encapsulation for regression module
BOOST_PYTHON_MODULE(StOptReg)
{


    Register<ArrayXXd>();
    Register<ArrayXd>();
    Register<ArrayXi>();


    using namespace boost::python;


    // Map regressors
    //***************
    class_<BaseRegressionWrap, std::shared_ptr<BaseRegressionWrap>, boost::noncopyable>("BaseRegression")
    .def("updateSimulations", pure_virtual(&BaseRegression::updateSimulations))
    .def("getCoordBasisFunction", pure_virtual(&BaseRegression::getCoordBasisFunction))
    .def("getCoordBasisFunctionMultiple", pure_virtual(&BaseRegression::getCoordBasisFunctionMultiple))
    .def("getAllSimulations", pure_virtual(&BaseRegression::getAllSimulations))
    .def("getAllSimulationsMultiple", pure_virtual(&BaseRegression::getAllSimulationsMultiple))
    .def("reconstruction", pure_virtual(&BaseRegression::reconstruction))
    .def("getValue", pure_virtual(&BaseRegression::getValue))
    .def("getAValue", pure_virtual(&BaseRegression::getAValue))
    .def("getNbSimul", &BaseRegression::getNbSimul)
    .def("getParticles", &BaseRegression::getParticlesRef, return_value_policy<copy_const_reference>()) // copy because send back a const
    .def("getDimension", &BaseRegression::getDimension)
    .def("getNumberOfFunction", &BaseRegression::getNumberOfFunction)
    .def("reconstructionMultiple", &BaseRegression::reconstructionMultiple)
    ;

    class_<LocalLinearRegression, std::shared_ptr< LocalLinearRegression>, bases<BaseRegression> > ("LocalLinearRegression")
    .def(init< const Eigen::ArrayXi &>())
    .def(init< const bool &, const shared_ptr< Eigen::ArrayXXd>  &, const Eigen::ArrayXi &> ())
    ;
    class_<LocalConstRegression, std::shared_ptr< LocalConstRegression>, bases<BaseRegression> > ("LocalConstRegression")
    .def(init<   const Eigen::ArrayXi &>())
    .def(init< const bool &, const shared_ptr< Eigen::ArrayXXd>  &, const Eigen::ArrayXi &> ())
    ;
    class_<LocalSameSizeLinearRegression, std::shared_ptr< LocalSameSizeLinearRegression>, bases<BaseRegression> > ("LocalSameSizeLinearRegression")
    .def(init<const Eigen::ArrayXd &, const Eigen::ArrayXd &, const  Eigen::ArrayXi & >())
    .def(init<const bool &,   const std::shared_ptr< Eigen::ArrayXXd > &,   const Eigen::ArrayXd &,  const Eigen::ArrayXd &,  const  Eigen::ArrayXi & > ())
    .def(init<const bool &,  const Eigen::ArrayXd &, const Eigen::ArrayXd &, const Eigen::ArrayXi & >())
    ;
    class_<LocalSameSizeConstRegression, std::shared_ptr< LocalSameSizeConstRegression>, bases<BaseRegression> > ("LocalSameSizeConstRegression")
    .def(init<const Eigen::ArrayXd &, const Eigen::ArrayXd &, const  Eigen::ArrayXi & >())
    .def(init<const bool &,   const std::shared_ptr< Eigen::ArrayXXd > &,   const Eigen::ArrayXd &,  const Eigen::ArrayXd &,  const  Eigen::ArrayXi & > ())
    .def(init<const bool &,  const Eigen::ArrayXd &, const Eigen::ArrayXd &, const Eigen::ArrayXi & >())
    ;
    class_<SparseRegression, std::shared_ptr<SparseRegression>, bases<BaseRegression> > ("SparseRegression")
    .def(init<  const int &, const Eigen::ArrayXd &, const int &>())
    .def(init< const bool &, const shared_ptr< Eigen::ArrayXXd > & ,  const int &, const Eigen::ArrayXd &, const int &> ())
    ;
    class_<GlobalRegression<Hermite>, std::shared_ptr<GlobalRegression<Hermite> >, bases<BaseRegression> > ("GlobalHermiteRegression")
    .def(init< const int &, const int & >())
    .def(init< const bool &, const std::shared_ptr< Eigen::ArrayXXd > & ,  const int & > ())
    ;
    class_<GlobalRegression<Canonical>, std::shared_ptr<GlobalRegression<Canonical> >, bases<BaseRegression> > ("GlobalCanonicalRegression")
    .def(init< const int &, const int & >())
    .def(init< const bool &, const std::shared_ptr< Eigen::ArrayXXd > & ,  const int & > ())
    ;
    class_<GlobalRegression<Tchebychev>, std::shared_ptr<GlobalRegression<Tchebychev> >, bases<BaseRegression> > ("GlobalTchebychevRegression")
    .def(init< const int &, const int & >())
    .def(init< const bool &, const std::shared_ptr< Eigen::ArrayXXd > & ,  const int & > ())
    ;


    // mapp continuation values with stock points
    class_<ContinuationValueWrap>("ContinuationValue", init <  const boost::shared_ptr< SpaceGrid >  & ,  const boost::shared_ptr< BaseRegression> & ,    const Eigen::ArrayXXd & > ())
    .def(init<   const boost::shared_ptr< BaseRegression> &>())
    .def("loadForSimulation", &ContinuationValueWrap::loadForSimulation)
    .def("loadCondExpForSimulation", &ContinuationValueWrap::loadCondExpForSimulation)
    .def("getAllSimulations", &ContinuationValueWrap::getAllSimulations)
    .def("getAllSimulations", &ContinuationValueWrap::getAllSimulationsInterp)
    .def("getASimulation", &ContinuationValueWrap::getASimulation)
    .def("getValue", &ContinuationValueWrap::getValue)
    .def("getGrid", &ContinuationValueWrap::getGrid)
    .def("getCondExp", &ContinuationValueWrap::getCondExp)
    .def("getNbSimul", &ContinuationValueWrap::getNbSimul)
    .def("getParticles", &ContinuationValueWrap::getParticles)
    ;

    // mapp continuation values with stock points
    class_<GridAndRegressedValueWrap>("GridAndRegressedValue", init <  const boost::shared_ptr< SpaceGrid >  & ,  const boost::shared_ptr< BaseRegression> & ,    const Eigen::ArrayXXd & > ())
    .def(init<   const boost::shared_ptr< SpaceGrid >  & , const boost::shared_ptr< BaseRegression> &>())
    .def(init<   const boost::shared_ptr< BaseRegression> &>())
    .def("getValue", &GridAndRegressedValueWrap::getValue)
    .def("getGrid", &GridAndRegressedValueWrap::getGrid)
    .def("getRegressor", &GridAndRegressedValueWrap::getRegressor)
    ;

    register_ptr_to_python< std::shared_ptr<BaseRegression > >();

}
