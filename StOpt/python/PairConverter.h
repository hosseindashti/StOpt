// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
// conversion
#ifndef PAIRCONVERTER_H
#define PAIRCONVERTER_H
#include <Python.h>
#include <memory>
#include <vector>
#include <map>

template< class T1, class T2>
struct pairSpec
{
    static PyObject *convert(const std::pair< std::vector< T1> , T2  >    &p_pair)
    {
        // put it in a list of list
        boost::python::list *l = new boost::python::list();
        boost::python::list *lInside = new boost::python::list();
        for (size_t i = 0; i < p_pair.first.size(); i++)
            (*lInside).append(p_pair.first[i]);
        (*l).append(*lInside);
        (*l).append(p_pair.second);
        return l->ptr();
    }
};

// conversion
template< class T1, class T2>
struct pairSpec1
{
    static PyObject *convert(const std::pair< T1 , T2  >    &p_pair)
    {
        // put it in a list of list
        boost::python::list *l = new boost::python::list();
        (*l).append(p_pair.first);
        (*l).append(p_pair.second);
        return l->ptr();
    }
};

// conversion
template< class T1, class T2>
struct pairSpec2
{
    static PyObject *convert(const std::pair< std::vector< std::shared_ptr< T1 > >, std::vector<  std::shared_ptr< T2 > > >  &p_pair)
    {
        // put it in a list of list
        boost::python::list *l = new boost::python::list();
        boost::python::list *l1Inside = new boost::python::list();
        boost::python::list *l2Inside = new boost::python::list();
        for (size_t i = 0; i < p_pair.first.size(); i++)
            (*l1Inside).append(*p_pair.first[i]);
        for (size_t i = 0; i < p_pair.second.size(); i++)
            (*l2Inside).append(*p_pair.second[i]);
        (*l).append(*l1Inside);
        (*l).append(*l2Inside);
        return l->ptr();
    }
};
#endif /* PAIRCONVERTER_H */
