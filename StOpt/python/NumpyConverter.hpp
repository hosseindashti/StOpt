//The MIT License (MIT)
//
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
// Created by David Evans on 6/14/2013.
// Modified by Xavier Warin 10/11/2014 (shared_ptr)
//                          15/05/2016 (check registry)
//

#ifndef _Converter_hpp
#define _Converter_hpp
#include <memory>
#include <boost/python/converter/registry.hpp>
#include <boost/python.hpp>
#include <boost/python/numeric.hpp>
#include <boost/numpy.hpp>
#include <Eigen/Dense>


template<class matrix_t>
struct ndarray_to_Matrix_converter
{
    typedef typename matrix_t::Scalar _Scalar;
    const static int _Rows = matrix_t::RowsAtCompileTime;
    const static int _Cols = matrix_t::ColsAtCompileTime;
    const static int _Options = matrix_t::Options;

    ndarray_to_Matrix_converter()
    {
        boost::python::converter::registry::push_back(
            &convertible,
            &construct,
            boost::python::type_id< matrix_t >()
        );
    }

    /**
     *  Test to see if we can convert this to the desired type; if not return zero.
     *  If we can convert, returned pointer can be used by construct().
     */
    static void *convertible(PyObject *p)
    {
        try
        {
            boost::python::object obj(boost::python::handle<>(boost::python::borrowed(p)));
            int Nmin = (_Rows > 1) + (_Cols > 1);
            std::unique_ptr<boost::numpy::ndarray> array;
            if (_Options == Eigen::RowMajor)
                array = std::unique_ptr<boost::numpy::ndarray>(
                            new boost::numpy::ndarray(
                                boost::numpy::from_object(obj, boost::numpy::dtype::get_builtin<_Scalar>(), Nmin, 2, boost::numpy::ndarray::C_CONTIGUOUS)
                            )
                        );
            else
                array = std::unique_ptr<boost::numpy::ndarray>(
                            new boost::numpy::ndarray(
                                boost::numpy::from_object(obj, boost::numpy::dtype::get_builtin<_Scalar>(), Nmin, 2, boost::numpy::ndarray::F_CONTIGUOUS)
                            )
                        );
            //scalar case
            if ((array->get_nd() == 0) && (_Rows > 1 || _Cols > 1)) return 0;
            //1d vector case
            if (array->get_nd() == 1)
            {
                if ((_Rows > 1) && (_Cols > 1)) return 0;
                if ((_Rows == 1) && ((_Cols != array->shape(0)) && (_Cols != Eigen::Dynamic))) return 0;
                if ((_Cols == 1) && ((_Rows != array->shape(0)) && (_Rows != Eigen::Dynamic))) return 0;
            }
            //2d vector case
            if (array->get_nd() == 2)
            {
                if (((_Rows != array->shape(0)) && (_Rows != Eigen::Dynamic)) || ((_Cols != array->shape(1)) && (_Cols != Eigen::Dynamic)))
                {
                    return 0;
                }
            }

            return array.release();
        }
        catch (boost::python::error_already_set &err)
        {
            boost::python::handle_exception();
            return 0;
        }
    }

    /**
     *  Finish the conversion by initializing the C++ object into memory prepared by Boost.Python.
     */
    static void construct(PyObject *obj, boost::python::converter::rvalue_from_python_stage1_data *data)
    {
        // Extract the array we passed out of the convertible() member function.
        std::unique_ptr<boost::numpy::ndarray> array(reinterpret_cast<boost::numpy::ndarray *>(data->convertible));
        // Find the memory block Boost.Python has prepared for the result.
        typedef boost::python::converter::rvalue_from_python_storage<matrix_t> storage_t;
        storage_t *storage = reinterpret_cast<storage_t *>(data);

        // Fill the result with the values from the NumPy array.
        int n = 1;
        int m = 1;
        switch (array->get_nd())
        {
        case 1:
            if (_Cols != 1)
            {
                n = 1;
                m = array->shape(0);
            }
            else
            {
                n = array->shape(0);
                m = 1;
            }
            break;
        case 2:
            n = array->shape(0);
            m = array->shape(1);
            break;

        default:
            break;
        }
        // Use placement new to initialize the result.
        matrix_t *mat = new(storage->storage.bytes) matrix_t();
        Eigen::Map<matrix_t > temp(reinterpret_cast<_Scalar *>(array->get_data()), n, m);
        *mat = temp;
        // Finish up.
        data->convertible = storage->storage.bytes;
    }
};

template<class matrix_t>
struct ndarray_to_MySharedPtr_Matrix_converter
{
    typedef typename matrix_t::Scalar _Scalar;
    const static int _Rows = matrix_t::RowsAtCompileTime;
    const static int _Cols = matrix_t::ColsAtCompileTime;
    const static int _Options = matrix_t::Options;

    ndarray_to_MySharedPtr_Matrix_converter()
    {
        boost::python::converter::registry::push_back(
            &ndarray_to_Matrix_converter<matrix_t>::convertible,
            &construct,
            boost::python::type_id< std::shared_ptr< matrix_t> >()
        );

    }


    /**
     *  Finish the conversion by initializing the C++ object into memory prepared by Boost.Python.
     */
    static void construct(PyObject *obj, boost::python::converter::rvalue_from_python_stage1_data *data)
    {
        // Extract the array we passed out of the convertible() member function.
        std::unique_ptr<boost::numpy::ndarray> array(reinterpret_cast<boost::numpy::ndarray *>(data->convertible));
        // Find the memory block Boost.Python has prepared for the result.
        typedef boost::python::converter::rvalue_from_python_storage< std::shared_ptr<matrix_t> > storage_t;
        storage_t *storage = reinterpret_cast<storage_t *>(data);

        // Fill the result with the values from the NumPy array.
        int n = 1;
        int m = 1;
        switch (array->get_nd())
        {
        case 1:
            if (_Cols != 1)
            {
                n = 1;
                m = array->shape(0);
            }
            else
            {
                n = array->shape(0);
                m = 1;
            }
            break;
        case 2:
            n = array->shape(0);
            m = array->shape(1);
            break;

        default:
            break;
        }
        // Use placement new to initialize the result.
        std::shared_ptr<matrix_t> *mat = new(storage->storage.bytes) std::shared_ptr<matrix_t>(std::make_shared<matrix_t>(n, m));
        Eigen::Map<matrix_t > temp(reinterpret_cast<_Scalar *>(array->get_data()), n, m);
        **mat = temp;
        // Finish up.
        data->convertible = storage->storage.bytes;
    }
};


template<class matrix_t>
struct Matrix_to_ndarray
{
    typedef typename matrix_t::Scalar _Scalar;
    const static int _Rows = matrix_t::RowsAtCompileTime;
    const static int _Cols = matrix_t::ColsAtCompileTime;
    const static int _Options = matrix_t::Options;

    static PyObject *convert(const matrix_t &M)
    {
        int n = M.rows();
        int m = M.cols();
        boost::python::tuple shape;
        boost::python::tuple stride;
        shape = boost::python::make_tuple(n, m);
        if (_Options == Eigen::RowMajor)
            stride = boost::python::make_tuple(m * sizeof(_Scalar), sizeof(_Scalar));
        else
            stride = boost::python::make_tuple(sizeof(_Scalar), n * sizeof(_Scalar));

        boost::numpy::ndarray a = boost::numpy::from_data(
                                      &M(0),
                                      boost::numpy::dtype::get_builtin<_Scalar>(),
                                      shape,
                                      stride,
                                      boost::python::object()
                                  );
        return boost::python::incref(a.copy().ptr());

    }
};

template <class matrix_t>
static void Register()
{
    boost::numpy::initialize();
    ndarray_to_Matrix_converter< matrix_t >();
    ndarray_to_MySharedPtr_Matrix_converter< matrix_t>();
    // ndarray_to_MapMatrix_converter< matrix_t >();
    boost::python::type_info info = boost::python::type_id< matrix_t >();
    const boost::python::converter::registration *reg = boost::python::converter::registry::query(info);
    if (reg == NULL)
    {
        boost::python::to_python_converter <
        matrix_t,
        Matrix_to_ndarray< matrix_t > > ();
    }
    else if ((*reg).m_to_python == NULL)
    {
        boost::python::to_python_converter <
        matrix_t,
        Matrix_to_ndarray< matrix_t > > ();
    }
}


#endif
