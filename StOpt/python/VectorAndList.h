// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef VECTORANDLIST_H
#define VECTORANDLIST_H
#include <memory>
#include <Python.h>

/// \brief Defines converter from list to c++ and c++ to list
// Vector to List
template< typename T>
struct VecToList
{
    /// \brief Function
    /// \param p_vec vector to convert to PyObject
    /// \return a PyObject
    static PyObject *convert(const std::vector< T> &p_vec)
    {
        boost::python::list *l = new boost::python::list();
        for (size_t i = 0; i < p_vec.size(); i++)
        {
            (*l).append(p_vec[i]);
        }
        return l->ptr();
    }
};



template< typename T>
struct VecToListShPtr
{
    /// \brief Function
    /// \param p_vec vector of shared_ptr to convert to PyObject
    /// \return a PyObject
    static PyObject *convert(const std::vector< std::shared_ptr< T> > &p_vec)
    {
        boost::python::list *l = new boost::python::list();
        for (size_t i = 0; i < p_vec.size(); i++)
            (*l).append(*p_vec[i]);

        return l->ptr();
    }
};



// list of objects to vector of shared_ptr
template< typename T>
std::vector< std::shared_ptr< T > > convertFromListShPtr(const boost::python::list &ns)
{
    std::vector< std::shared_ptr< T> > ret(len(ns));
    for (int i = 0; i < len(ns); ++i)
    {
        T local = boost::python::extract<T>(ns[i]); // temporary necessary for ICC and Eigen 3.3
        ret[i] = std::make_shared<T>(local);
        /* ret[i] = std::make_shared<T>(boost::python::extract<T>(ns[i])); */
    }
    return ret;
}


// converter list of objects to vector of shared_ptr
template< typename T>
std::vector<T >  convertFromList(const boost::python::list &ns)
{
    std::vector<  T > ret;
    ret.reserve(len(ns));
    for (int i = 0; i < len(ns); ++i)
        ret.push_back(boost::python::extract<T>(ns[i]));
    return ret;
}

// same but send back a boost shared_ptr
template< typename T>
std::shared_ptr< std::vector<T > >  convertFromListToShared(const boost::python::list &ns)
{
    std::shared_ptr< std::vector<T > > ret = std::make_shared< std::vector<T> >() ;
    ret->reserve(len(ns));
    for (int i = 0; i < len(ns); ++i)
        ret->push_back(boost::python::extract<T>(ns[i]));
    return ret;
}

#endif /* VECTORANDLIST_H */
