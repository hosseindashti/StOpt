// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef BACKWARDFORWARDSDDP_H
#define BACKWARDFORWARDSDDP_H
#ifdef USE_MPI
#include <boost/mpi.hpp>
#include <boost/lexical_cast.hpp>
#endif
#include "geners/BinaryFileArchive.hh"
#include "geners/Record.hh"
#include "StOpt/sddp/LocalLinearRegressionForSDDP.h"
#include "StOpt/sddp/SimulatorSDDPBase.h"
#include "StOpt/sddp/forwardSDDP.h"
#include "StOpt/sddp/backwardSDDP.h"
#include "StOpt/sddp/OptimizerSDDPBase.h"
#include "StOpt/sddp/SDDPVisitedStatesGeners.h"

/** \file backwardForwardSDDP.h
 * \brief alternates forward and backward resolution
 * \author Xavier Warin
 */
namespace StOpt
{


/// \brief Achieve forward and backward sweep by SDDP
/// \param  p_optimizer           defines the optimiser necessary to optimize a step for one simulation solving a LP
/// \param  p_nbSimulCheckForSimu defines the number of simulations to check convergence
/// \param  p_initialState        initial state at the beginning of simulation
/// \param  p_finalCut            object of final cuts
/// \param  p_dates               vector of exercised dates, last dates correspond to the final cut object
/// \param  p_meshForReg          number of mesh for regression in each direction
/// \param  p_nameRegressor       name of the archive to store regressors
/// \param  p_nameCut             name of the archive to store cuts
/// \param  p_nameVisitedStates   name of the archive to store visited states
/// \param  p_iter                maximum iteration of SDDP, on return the number of iterations achieved
/// \param  p_accuracy            accuracy asked , on return estimation of accuracy achieved (expressed in %)
/// \param  p_nStepConv           every p_nStepConv convergence is checked
std::pair<double, double> backwardForwardSDDP(std::shared_ptr<OptimizerSDDPBase>    &p_optimizer,
        const int   &p_nbSimulCheckForSimu,
        const Eigen::ArrayXd &p_initialState,
        const SDDPFinalCut &p_finalCut,
        const Eigen::ArrayXd &p_dates,
        const Eigen::ArrayXi &p_meshForReg,
        const std::string &p_nameRegressor,
        const std::string &p_nameCut,
        const std::string &p_nameVisitedStates,
        int &p_iter,
        double &p_accuracy,
        const int &p_nStepConv)

{
    // get back simulators
    std::shared_ptr<SimulatorSDDPBase> simulatorForOptim = p_optimizer->getSimulatorBackward();
    std::shared_ptr<SimulatorSDDPBase> simulatorForSim = p_optimizer->getSimulatorForward();

#if USE_MPI
    boost::mpi::communicator world;
    int iTask = world.rank();
#else
    int iTask = 0;
#endif
    assert(p_dates(0) == 0.);
    if (iTask == 0)
    {
        // create archive for regressors
        gs::BinaryFileArchive archiveForRegressor(p_nameRegressor.c_str(), "w");
        // create a first set of admissible states
        gs::BinaryFileArchive archiveForInitialState(p_nameVisitedStates.c_str(), "w");
        // vector of states
        std::vector< std::unique_ptr< SDDPVisitedStates > > vecSetOfStates(p_dates.size() - 1);
        // create regressor : each date except the last
        for (int idate = p_dates.size() - 2; idate >  0; --idate)
        {
            simulatorForOptim->updateDates(p_dates(idate));
            // initial regressor
            std::shared_ptr<Eigen::ArrayXXd> particlesCurrent(new Eigen::ArrayXXd(simulatorForOptim->getParticles()));
            LocalLinearRegressionForSDDP regCurrent(false, particlesCurrent, p_meshForReg);
            archiveForRegressor << gs::Record(regCurrent, "Regressor", "Top");
            std::unique_ptr< SDDPVisitedStates> setOfStates(new SDDPVisitedStates);
            // offset  in admissible dates
            std::shared_ptr< Eigen::ArrayXd > anAdmissibleState = p_optimizer->oneAdmissibleState(p_dates(idate + 1));
            setOfStates->addVisitedStateForAll(anAdmissibleState, regCurrent);
            vecSetOfStates[idate] = std::move(setOfStates);
        }
        simulatorForOptim->updateDates(p_dates(0));
        std::shared_ptr<Eigen::ArrayXXd> particlesInit(new Eigen::ArrayXXd(simulatorForOptim->getParticles()));
        LocalLinearRegressionForSDDP regInit(true, particlesInit, p_meshForReg);
        archiveForRegressor << gs::Record(regInit, "Regressor", "Top");
        std::unique_ptr< SDDPVisitedStates> setOfStates(new SDDPVisitedStates);
        std::shared_ptr< Eigen::ArrayXd > anAdmissibleState =  std::shared_ptr< Eigen::ArrayXd >(new Eigen::ArrayXd(p_initialState));
        setOfStates->addVisitedStateForAll(anAdmissibleState, regInit);
        vecSetOfStates[0] = std::move(setOfStates);

        // archive initial admissible states (forward order)
        for (int idate = 0; idate <= vecSetOfStates.size() - 1; ++idate)
            archiveForInitialState << gs::Record(*vecSetOfStates[idate], "States", "Top");
    }

#if USE_MPI
    world.barrier();
#endif
    int iterMax = p_iter;
    p_iter = 0;
    double accuracy = p_accuracy;
    p_accuracy = 1e10;
    // archive to read regressor : currently all processor reads the regression
    gs::BinaryFileArchive archiveReadRegressor(p_nameRegressor.c_str(), "r");
    // archive for cuts read write
    std::unique_ptr<gs::BinaryFileArchive> archiveForCuts;

    // only create for first task
    if (iTask == 0)
        archiveForCuts = std::move(std::unique_ptr<gs::BinaryFileArchive>(new gs::BinaryFileArchive(p_nameCut.c_str(), "w+")));

    // to store  all backward values
    std::vector<double> backwardValues(iterMax);
    // forward value
    double forwardValueForConv;
    int istep = 0;
    // store evolution of convergence
    double backwardMinusForwardPrev = 0;
    while ((accuracy < p_accuracy) && (p_iter < iterMax))
    {
        // increase step
        istep += 1;
        //  actualize time for simulators
        simulatorForOptim->resetTime();
        simulatorForSim->resetTime();
#if USE_MPI
        world.barrier();
#endif
        // backward sweep
        backwardValues[p_iter] = backwardSDDP(p_optimizer , simulatorForOptim, p_dates,  p_initialState, p_finalCut, archiveReadRegressor,
                                              p_nameVisitedStates , archiveForCuts);
#if USE_MPI
        world.barrier();
#endif
        // forward sweep
        bool   bIncreaseCut  = true;

        double valueForward = forwardSDDP(p_optimizer , simulatorForSim, p_dates, p_initialState, p_finalCut, bIncreaseCut, archiveReadRegressor,
                                          archiveForCuts, p_nameVisitedStates);
#if USE_MPI
        world.barrier();
#endif
        if ((istep == p_nStepConv) || (p_iter == 0))
        {
            istep = 0;
            int oldParticleNb = simulatorForSim->getNbSimul();
            int oldSampleSim = simulatorForSim->getNbSample();
            simulatorForSim->updateSimulationNumberAndResetTime(simulatorForOptim->getNbSimul(), p_nbSimulCheckForSimu);
            bIncreaseCut = false;
            forwardValueForConv =  forwardSDDP(p_optimizer , simulatorForSim, p_dates, p_initialState, p_finalCut, bIncreaseCut, archiveReadRegressor,
                                               archiveForCuts, p_nameVisitedStates);
            p_accuracy  = std::fabs((backwardValues[p_iter] - forwardValueForConv) / forwardValueForConv);
            simulatorForSim->updateSimulationNumberAndResetTime(oldParticleNb, oldSampleSim);
            if (iTask == 0)
                std::cout << " ACCURACY " << p_accuracy  << " Backward " << backwardValues[p_iter] << " Forward " << forwardValueForConv << "p_iter "  << p_iter << " accuracy " <<  p_accuracy << std::endl ;
            double backwardMinusForward = backwardValues[p_iter] - forwardValueForConv;
            if (p_iter > 0)
            {
                if (backwardMinusForward * backwardMinusForwardPrev < 0)
                {
                    if (iTask == 0)
                        std::cout << " Curve are crossing : increase sample and simulations to get more accurate solution, decrease step for checking convergence" << std::endl ;
                    // exit
                    p_accuracy = 0.;
                }
            }
            backwardMinusForwardPrev = backwardMinusForward;
        }
        p_iter += 1;
    }

#if USE_MPI
    world.barrier();
#endif
    return std::make_pair(backwardValues[p_iter - 1], forwardValueForConv);

}
}
#endif /* BACKWARDFORWARDSDDP_H */
