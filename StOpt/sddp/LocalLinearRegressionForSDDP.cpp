// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#include <vector>
#include <memory>
#include <iostream>
#include <Eigen/Dense>
#include "StOpt/regression/meshCalculationLocalRegression.h"
#include "StOpt/regression/localLinearMatrixOperation.h"
#include "StOpt/sddp/LocalLinearRegressionForSDDP.h"

using namespace std;
using namespace Eigen;

namespace StOpt
{


int LocalLinearRegressionForSDDP::getMeshNumberAssociatedTo(const ArrayXd &p_oneParticle) const
{
    if ((m_nbMesh.size() > 0) && (!m_bZeroDate))
        return localLinearParticleToMesh(p_oneParticle , m_mesh1D);
    else
        return 0;
}


void  LocalLinearRegressionForSDDP::evaluateSimulBelongingToCell()
{
    int nbCells = getNumberOfCells();
    m_simulBelongingToCell.resize(nbCells);
    if (m_particles->cols() > 0)
    {
        for (int icell = 0; icell < nbCells; ++icell)
        {
            m_simulBelongingToCell[icell] = shared_ptr< vector< int> >(new vector< int>());
            m_simulBelongingToCell[icell]->reserve(2 * m_particles->cols() / nbCells);
        }
        for (int is = 0; is <  m_simToCell.size(); ++is)
            m_simulBelongingToCell[m_simToCell(is)]->push_back(is);
    }
    else
    {
        m_simulBelongingToCell[0] = shared_ptr< vector< int> >(new vector< int>(1));
        (*m_simulBelongingToCell[0])[0] = 0;
    }
}

ArrayXd LocalLinearRegressionForSDDP::getCoordBasisFunctionOneCell(const int &p_iCell, const ArrayXd &p_fToRegress) const
{
    if ((!m_bZeroDate) && (m_nbMesh.size() != 0))
    {
        Map<const ArrayXXd>  fToRegress2D(p_fToRegress.data(), 1, p_fToRegress.size());
        ArrayXXd secMember2D = localLinearSecondMemberCalculationOneCell(*m_particles, *(m_simulBelongingToCell[p_iCell]) , m_mesh.col(p_iCell), fToRegress2D);
        Map<const ArrayXd > secMember(secMember2D.data(), secMember2D.size());
        return localLinearCholeskiInversionOneCell(p_iCell, m_matReg, m_diagReg, secMember);
    }
    else
    {
        ArrayXd retAverage(1);
        retAverage(0) = p_fToRegress.mean();
        return retAverage;
    }
}

ArrayXXd LocalLinearRegressionForSDDP::getCoordBasisFunctionMultipleOneCell(const int &p_iCell, const ArrayXXd &p_fToRegress) const
{
    if ((!m_bZeroDate) && (m_nbMesh.size() != 0))
    {
        ArrayXXd secMember = localLinearSecondMemberCalculationOneCell(*m_particles, *(m_simulBelongingToCell[p_iCell]) , m_mesh.col(p_iCell), p_fToRegress);
        // normalization
        secMember /= m_particles->cols();
        ArrayXXd regFunc(p_fToRegress.rows(), secMember.cols());
        for (int nsm = 0; nsm <  p_fToRegress.rows(); ++nsm)
        {
            ArrayXd secMemberLoc(secMember.cols());
            secMemberLoc = secMember.row(nsm);
            regFunc.row(nsm) = localLinearCholeskiInversionOneCell(p_iCell, m_matReg, m_diagReg, secMemberLoc).transpose();
        }
        return regFunc;
    }
    else
    {
        ArrayXXd retAverage(p_fToRegress.rows(), 1);
        for (int nsm = 0; nsm <  p_fToRegress.rows(); ++nsm)
            retAverage.row(nsm).setConstant(p_fToRegress.row(nsm).mean());
        return retAverage;
    }

}

ArrayXd LocalLinearRegressionForSDDP::getParticle(const int &p_iPart) const
{
    assert((p_iPart == 0) || (p_iPart < m_particles->cols()));
    if ((p_iPart == 0) && (m_particles->size() == 0))
        return ArrayXd();
    else
        return m_particles->col(p_iPart);
}


ArrayXd  LocalLinearRegressionForSDDP::getValuesOneCell(const ArrayXd &p_oneParticle, const int p_cell, const ArrayXXd   &p_foncBasisCoef)
{
    if ((!m_bZeroDate) && (m_nbMesh.size() != 0))
    {
        return localLinearReconstructionOnePointOneCell(p_oneParticle, m_mesh.col(p_cell), p_foncBasisCoef);
    }
    else
    {
        return  p_foncBasisCoef.col(0);
    }

}
}

