// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef FORWARDSDDDP_H
#define FORWARDSDDDP_H
#include <memory>
#ifdef USE_MPI
#include <boost/mpi.hpp>
#endif
#include "geners/BinaryFileArchive.hh"
#include "geners/Record.hh"
#include "geners/Reference.hh"
#include "StOpt/sddp/LocalLinearRegressionForSDDP.h"
#include "StOpt/sddp/LocalLinearRegressionForSDDPGeners.h"
#include "StOpt/sddp/SDDPLocalLinearCut.h"
#include "StOpt/sddp/SDDPFinalCut.h"
#include "StOpt/sddp/OptimizerSDDPBase.h"
#include "StOpt/sddp/SimulatorSDDPBase.h"

/** \file forwardSDDP.h
 * \brief On sequence of forward resolution by SDDP
 * \author Xavier Warin
 */

namespace StOpt
{
/// \brief Achieve  a forward sweep for SDDP
/// \param p_optimizer              object defining a transition step for SDDP
/// \param p_simulator              simulates uncertainties for regressions, inflows etc....
/// \param p_dates                  vector of exercised dates
/// \param p_initialState           initial state at the beginning of simulation
/// \param p_finalCut               storing final cuts
/// \param p_archiveRegresssor      archive with regressor objects
/// \param p_archiveCutToRead       archive storing cuts visited
/// \param p_nameVisitedStates      name of the archive used to store   visited states
/// \param p_bIncreaseCut           true if this simulation part create visited state for cut
/// \return value obtained with this simulations
double	forwardSDDP(std::shared_ptr<OptimizerSDDPBase>    &p_optimizer ,
                    std::shared_ptr<SimulatorSDDPBase>  &p_simulator,
                    const Eigen::ArrayXd &p_dates,
                    const Eigen::ArrayXd &p_initialState,
                    const SDDPFinalCut &p_finalCut,
                    const bool   &p_bIncreaseCut  ,
                    gs::BinaryFileArchive &p_archiveRegresssor,
                    std::unique_ptr<gs::BinaryFileArchive> &p_archiveCutToRead,
                    const std::string &p_nameVisitedStates)
{

    // to store cuts
    std::unique_ptr<gs::BinaryFileArchive> arVisitedStates;
#ifdef USE_MPI
    boost::mpi::communicator world;
    int nbTask = world.size();
    int iTask = world.rank();
#else
    int iTask = 0;
    int nbTask = 1 ;
#endif
    if ((iTask == 0) && (p_bIncreaseCut))
        arVisitedStates = std::move(std::unique_ptr<gs::BinaryFileArchive>(new gs::BinaryFileArchive(p_nameVisitedStates.c_str(), "w")));

    // get number of sample used
    int nbSample = p_simulator->getNbSample();
    int nsimPProc = (int)(p_simulator->getNbSimul() * nbSample / nbTask);
    int nRest = (p_simulator->getNbSimul() * nbSample) % nbTask;
    int iLPFirst = iTask * nsimPProc + (iTask < nRest ? iTask : nRest);
    int iLPLast  = iLPFirst + nsimPProc + (iTask < nRest ? 1 : 0);
    // to store states visited
    Eigen::ArrayXXd statePrev(p_initialState.size(), iLPLast - iLPFirst);
    for (int is = 0; is < iLPLast - iLPFirst; ++is)
        statePrev.col(is) = p_initialState ;
    // to store gain
    double gainAccumulator = 0;
    for (int idate = 0; idate < p_dates.size() - 1; ++idate)
    {
        // update new date
        p_optimizer->updateDates(p_dates(idate), p_dates(idate + 1));
        p_simulator->updateDates(p_dates(idate));

        // read condition expectation operator
        std::shared_ptr<LocalLinearRegressionForSDDP> regressor(std::move(gs::Reference< LocalLinearRegressionForSDDP >(p_archiveRegresssor , "Regressor", "Top").get(p_dates.size() - 2 - idate)));

        // create SDPPCut object
        std::unique_ptr< SDDPCutBase > linCut;
        if (idate <  p_dates.size() - 2)
            linCut   = std::move(std::unique_ptr< SDDPCutBase >(new SDDPLocalLinearCut(idate, nbSample, regressor)));
        else
            linCut = std::move(std::unique_ptr< SDDPCutBase >(new SDDPFinalCut(p_finalCut)));

        // load cuts
        linCut->loadCuts(p_archiveCutToRead);

        // to store visited states
        SDDPVisitedStates setOfStates;
        int isim;
        #pragma omp parallel  for schedule(dynamic)  private(isim) reduction(+:gainAccumulator)
        for (isim = 0; isim < iLPLast - iLPFirst; ++isim)
        {
            // simulation number according to simulator
            int isimul = (iLPFirst + isim) / nbSample;
            // sample number
            int isample = (isim + iLPFirst) % nbSample;
            // get back particle associated
            Eigen::ArrayXd aParticle(p_simulator->getOneParticle(isimul));

            std::shared_ptr<Eigen::ArrayXd > newState(new Eigen::ArrayXd(statePrev.col(isim))) ;
            std::shared_ptr<Eigen::ArrayXd > newStateToStore(new Eigen::ArrayXd(statePrev.col(isim))) ;
            double gainOrCost = p_optimizer->oneStepForward(aParticle, *newState, *newStateToStore, linCut, isample);
            // accumulate gain
            gainAccumulator += gainOrCost;

            // get back mesh number associated to particle
            int iMeshAssociated  = regressor->getMeshNumberAssociatedTo(aParticle);

            // add state and associated mesh
            if (p_bIncreaseCut)
                setOfStates.addVisitedState(newStateToStore, aParticle, *regressor);
            // update state
            statePrev.col(isim) = *newState;
        }
        // store if root
#ifdef USE_MPI
        if (idate <  p_dates.size() - 2)
            if (p_bIncreaseCut)
                setOfStates.sendToRoot();
        if (iTask == 0)
#endif
            if (p_bIncreaseCut)
                *arVisitedStates << gs::Record(setOfStates, "States", "Top");
    }
#ifdef USE_MPI
    gainAccumulator = boost::mpi::all_reduce(world, gainAccumulator, std::plus<double>());
#endif
    return gainAccumulator / (p_simulator->getNbSimul() * nbSample);
}
}
#endif
