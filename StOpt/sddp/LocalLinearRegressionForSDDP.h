// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef LOCALLINEARREGRESSIONFORSDDP_H
#define LOCALLINEARREGRESSIONFORSDDP_H
#include "StOpt/regression/LocalLinearRegression.h"

/** \file   LocalLinearRegressionForSDDP.h
 *  \brief  Conditional expectation by local regression extended for SDDP
  *  \author Xavier Warin
 */
namespace StOpt
{

/// \class LocalLinearRegressionForSDDP LocalLinearRegressionForSDDP.h
/// Special case of linear regression for SDDP
class LocalLinearRegressionForSDDP: public LocalLinearRegression
{
private :

    std::vector<  std::shared_ptr< std::vector< int> > > m_simulBelongingToCell; ///< Utility  : to each cell defines the particles lying inside


public :

    /// \brief Default constructor
    LocalLinearRegressionForSDDP() {}


    /// \brief First constructor for object constructed at each time step
    /// \param  p_bZeroDate    first date is 0?
    /// \param  p_particles    particles used for the meshes.
    ///                        First dimension  : dimension of the problem,
    ///                        second dimension : the  number of particles
    /// \param  p_nbMesh       discretization in each direction
    LocalLinearRegressionForSDDP(const bool &p_bZeroDate,
                                 const std::shared_ptr< Eigen::ArrayXXd> &p_particles,
                                 const Eigen::ArrayXi   &p_nbMesh) : LocalLinearRegression(p_bZeroDate, p_particles, p_nbMesh)
    {
        evaluateSimulBelongingToCell();
    }

    /// \brief Second constructor
    /// \param  p_bZeroDate        first date is 0?
    /// \param  p_particles        particles used for the meshes.
    ///                            First dimension  : dimension of the problem,
    ///                            second dimension : the  number of particles
    /// \param  p_mesh             for each cell and each direction defines the min and max coordinate of a cell
    /// \param  p_mesh1D           meshes in each direction
    /// \param  p_simToCell        For each simulation gives its global mesh number
    /// \param  p_matReg           regression matrix factorized
    /// \param  p_diagReg          diagonal of factorized matrix
    /// \param  p_simulBelongingToCell  To each cell associate the set of all particles belonging to this cell
    LocalLinearRegressionForSDDP(const bool &p_bZeroDate,
                                 const std::shared_ptr<Eigen::ArrayXXd > &p_particles,
                                 const Eigen::Array< std::array< double, 2> , Eigen::Dynamic, Eigen::Dynamic > &p_mesh,
                                 const std::vector< std::shared_ptr< Eigen::ArrayXd > > &p_mesh1D,
                                 const Eigen::ArrayXi &p_simToCell,
                                 const Eigen::ArrayXXd   &p_matReg,
                                 const Eigen::ArrayXXd   &p_diagReg,
                                 const std::vector< std::shared_ptr< std::vector< int> > > &p_simulBelongingToCell) :
        LocalLinearRegression(p_bZeroDate, p_mesh1D)
    {
        m_mesh = p_mesh;
        m_particles = p_particles;
        m_simToCell = p_simToCell;
        m_matReg = p_matReg;
        m_diagReg = p_diagReg ;
        m_simulBelongingToCell = p_simulBelongingToCell;
    }



    /// \brief to a particle gives its cell number
    /// \param p_oneParticle  One point
    int getMeshNumberAssociatedTo(const Eigen::ArrayXd &p_oneParticle) const ;

    /// \brief To a particle get back the cell it belongs to
    /// \param p_isim particle number
    /// \return cell number it belongs to
    int getCellAssociatedToSim(const  int &p_isim) const
    {
        return  m_simToCell(p_isim);
    }

    /// \brief calculate a vector of vector of points giving for each cell the vector of points belonging to this cell
    void evaluateSimulBelongingToCell();

    /// \brief conditional expectation basis function coefficient calculation for a special cell
    /// \param  p_iCell     cell number
    /// \param  p_fToRegress  function to regress associated to each simulation used in optimization and corresponding to the cell
    /// \return regression coordinates on the basis  (size : the dimension of the problem plus one)
    /// @{
    Eigen::ArrayXd getCoordBasisFunctionOneCell(const int &p_iCell , const Eigen::ArrayXd &p_fToRegress) const;
    Eigen::ArrayXXd getCoordBasisFunctionMultipleOneCell(const int &p_iCell , const Eigen::ArrayXXd &p_fToRegress) const ;
    ///@}

    /// \brief get back the particle number
    int getParticleNumber() const
    {
        return m_particles->cols();
    }

    /// \brief get particles belonging all mesh
    inline  const std::vector<  std::shared_ptr< std::vector< int> > >    &getSimulBelongingToCell() const
    {
        return m_simulBelongingToCell;
    }


    /// \brief get back particle by its number
    /// \param p_iPart   particle number
    /// \return the particle (if no particle, send back an empty array)
    Eigen::ArrayXd getParticle(const int &p_iPart) const ;


    /// \brief Given a particle and the coordinates of the mesh it belongs to, get back the conditional expectation
    /// \param p_oneParticle  The particle generated
    /// \param p_cell      the cell it belongs to
    /// \param p_foncBasisCoef  function basis on the current cell (the row is the number of reconstruction to achieve, the columns the number of function basis)
    /// \return send back the array of conditional expectations
    Eigen::ArrayXd getValuesOneCell(const Eigen::ArrayXd &p_oneParticle, const int p_cell, const Eigen::ArrayXXd   &p_foncBasisCoef);


};
}
#endif /*LOCALLINEARREGRESSIONFORSDDP_H*/
