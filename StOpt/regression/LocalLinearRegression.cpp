// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#include <vector>
#include <memory>
#include <iostream>
#include <Eigen/Dense>
#include "StOpt/regression/LocalLinearRegression.h"
#include "StOpt/regression/meshCalculationLocalRegression.h"
#include "StOpt/regression/localLinearMatrixOperation.h"
#include "StOpt/core/grids/InterpolatorSpectral.h"

using namespace std;
using namespace Eigen;

namespace StOpt
{

LocalLinearRegression::LocalLinearRegression(const ArrayXi &p_nbMesh): LocalRegression(p_nbMesh) {}

LocalLinearRegression::LocalLinearRegression(const bool &p_bZeroDate,
        const shared_ptr< ArrayXXd>  &p_particles,
        const ArrayXi &p_nbMesh) : LocalRegression(p_bZeroDate, p_particles, p_nbMesh)
{
    if ((!m_bZeroDate) && (p_nbMesh.size() != 0))
    {
        int nbCell = m_mesh.cols() ;
        int iMatrixSize =  m_particles->rows() + 1;
        // regression matrix
        m_matReg = localLinearMatrixCalculation(*m_particles, m_simToCell, m_mesh);
        // factorize
        m_diagReg.resize(iMatrixSize, nbCell);
        // utilitary
        Array<bool, Dynamic, 1> bSingular(nbCell);
        localLinearCholeski(m_matReg, m_diagReg, bSingular) ;
    }
}

LocalLinearRegression:: LocalLinearRegression(const   LocalLinearRegression &p_object): LocalRegression(p_object), m_matReg(p_object.getMatReg()), m_diagReg(p_object.getDiagReg())

{}


void LocalLinearRegression::updateSimulations(const bool &p_bZeroDate, const shared_ptr< ArrayXXd>  &p_particles)
{
    BaseRegression::updateSimulationsBase(p_bZeroDate, p_particles);
    m_simToCell.resize(p_particles->cols());
    if ((!m_bZeroDate) && (m_nbMesh.size() != 0))
    {
        meshCalculationLocalRegression(*m_particles, m_nbMesh , m_simToCell, m_mesh, m_mesh1D);

        int nbCell = m_mesh.cols() ;
        int iMatrixSize =  m_particles->rows() + 1;
        // regression matrix
        m_matReg = localLinearMatrixCalculation(*m_particles, m_simToCell, m_mesh);
        // factorize
        m_diagReg.resize(iMatrixSize, nbCell);
        // utilitary
        Array<bool, Dynamic, 1> bSingular(nbCell);
        localLinearCholeski(m_matReg, m_diagReg, bSingular) ;
    }
    else
    {
        m_simToCell.setConstant(0);
    }
}

ArrayXd LocalLinearRegression::getCoordBasisFunction(const ArrayXd &p_fToRegress) const
{
    if ((!m_bZeroDate) && (m_nbMesh.size() != 0))
    {
        Map<const ArrayXXd>  fToRegress2D(p_fToRegress.data(), 1, p_fToRegress.size());
        ArrayXXd secMember2D = localLinearSecondMemberCalculation(*m_particles, m_simToCell, m_mesh, fToRegress2D);
        Map<const ArrayXd > secMember(secMember2D.data(), secMember2D.size());
        return localLinearCholeskiInversion(m_matReg, m_diagReg, secMember);
    }
    else
    {
        ArrayXd retAverage(1);
        retAverage(0) = p_fToRegress.mean();
        return retAverage;
    }
}

ArrayXXd LocalLinearRegression::getCoordBasisFunctionMultiple(const ArrayXXd &p_fToRegress) const
{
    if ((!m_bZeroDate) && (m_nbMesh.size() != 0))
    {
        ArrayXXd secMember = localLinearSecondMemberCalculation(*m_particles, m_simToCell, m_mesh, p_fToRegress);
        ArrayXXd regFunc(p_fToRegress.rows(), secMember.cols());
        for (int nsm = 0; nsm <  p_fToRegress.rows(); ++nsm)
        {
            ArrayXd secMemberLoc(secMember.cols());
            secMemberLoc = secMember.row(nsm);
            regFunc.row(nsm) = localLinearCholeskiInversion(m_matReg, m_diagReg, secMemberLoc).transpose();
        }
        return regFunc;
    }
    else
    {
        ArrayXXd retAverage(p_fToRegress.rows(), 1);
        for (int nsm = 0; nsm <  p_fToRegress.rows(); ++nsm)
            retAverage.row(nsm).setConstant(p_fToRegress.row(nsm).mean());
        return retAverage;
    }

}

ArrayXd LocalLinearRegression::reconstruction(const ArrayXd &p_basisCoefficients) const
{
    if ((!m_bZeroDate) && (m_nbMesh.size() != 0))
    {
        Map<const ArrayXXd> BasisCoefficients(p_basisCoefficients.data(), 1, p_basisCoefficients.size());
        return localLinearReconstruction(*m_particles, m_simToCell, m_mesh, BasisCoefficients).row(0);
    }
    else
        return ArrayXd::Constant(m_simToCell.size(), p_basisCoefficients(0));
}

ArrayXXd LocalLinearRegression::reconstructionMultiple(const ArrayXXd &p_basisCoefficients) const
{
    if ((!m_bZeroDate) && (m_nbMesh.size() != 0))
    {
        return localLinearReconstruction(*m_particles, m_simToCell, m_mesh, p_basisCoefficients);
    }
    else
    {
        ArrayXXd retValue(p_basisCoefficients.rows(), m_simToCell.size());
        for (int nsm = 0; nsm < p_basisCoefficients.rows(); ++nsm)
            retValue.row(nsm).setConstant(p_basisCoefficients(nsm, 0));
        return retValue ;
    }
}

double LocalLinearRegression::reconstructionASim(const int &p_isim , const ArrayXd   &p_basisCoefficients) const
{
    if ((!m_bZeroDate) && (m_nbMesh.size() != 0))
    {
        return localLinearReconstructionASim(p_isim , *m_particles, m_simToCell, m_mesh, p_basisCoefficients);
    }
    else
    {
        return p_basisCoefficients(0);
    }
}


ArrayXd LocalLinearRegression::getAllSimulations(const ArrayXd &p_fToRegress) const
{
    if ((m_bZeroDate) || (m_nbMesh.size() == 0))
        return ArrayXd::Constant(p_fToRegress.size(), p_fToRegress.mean());

    Map<const ArrayXXd>  fToRegress2D(p_fToRegress.data(), 1, p_fToRegress.size());
    ArrayXXd BasisCoefficients = getCoordBasisFunctionMultiple(fToRegress2D);
    ArrayXXd  condEspectationValues = localLinearReconstruction(*m_particles, m_simToCell, m_mesh, BasisCoefficients);
    return condEspectationValues.row(0);
}

ArrayXXd LocalLinearRegression::getAllSimulationsMultiple(const ArrayXXd &p_fToRegress) const
{
    if ((m_bZeroDate) || (m_nbMesh.size() == 0))
    {
        ArrayXXd ret(p_fToRegress.rows(), p_fToRegress.cols());
        for (int ism = 0; ism < p_fToRegress.rows(); ++ism)
            ret.row(ism).setConstant(p_fToRegress.row(ism).mean());
        return ret;
    }
    ArrayXXd BasisCoefficients = getCoordBasisFunctionMultiple(p_fToRegress);
    return localLinearReconstruction(*m_particles, m_simToCell, m_mesh, BasisCoefficients);
}

double LocalLinearRegression::getValue(const ArrayXd &p_coordinates, const ArrayXd &p_coordBasisFunction) const
{
    if ((!m_bZeroDate) && (m_nbMesh.size() != 0))
    {
        Map<const ArrayXXd> coordBasisFunction2D(p_coordBasisFunction.data(), 1, p_coordBasisFunction.size());
        return localLinearReconstructionOnePoint(p_coordinates, m_mesh1D, coordBasisFunction2D)(0);
    }
    else
    {
        return p_coordBasisFunction(0);
    }
}

double LocalLinearRegression::getAValue(const ArrayXd &p_coordinates,  const ArrayXd &p_ptOfStock,
                                        const vector< shared_ptr<InterpolatorSpectral> > &p_interpFuncBasis) const
{
    if ((!m_bZeroDate) && (m_nbMesh.size() != 0))
    {
        return localLinearReconsOnePointSimStock(p_coordinates, p_ptOfStock, p_interpFuncBasis, m_mesh1D);
    }
    else
    {
        return p_interpFuncBasis[0]->apply(p_ptOfStock);
    }
}
}
