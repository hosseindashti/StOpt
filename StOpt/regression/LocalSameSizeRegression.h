// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef LOCALSAMESIZEREGRESSION_H
#define LOCALSAMESIZEREGRESSION_H
#include <vector>
#include <memory>
#include <array>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <Eigen/Dense>
#include "StOpt/regression/BaseRegression.h"
#include "StOpt/core/grids/InterpolatorSpectral.h"

/** \file   LocalSameSizeRegression.h
 *  \brief  Base class for local regressions with size of constant size
 *          Useful mostly to check theoretical convergence rate obtained.
 *  \author Xavier Warin
 */
namespace StOpt
{
/**
 * \defgroup LocalSameSize Piecewise regression with mesh of same size
 * \brief It implements local local regression
  *@{
 */
/// \class LocalSameSizeRegression LocalSameSizeRegression.h
/// To be used in Monte Carlo methods  regression on each cell with cells with same size
class LocalSameSizeRegression : public BaseRegression
{
protected :

    Eigen::ArrayXd m_lowValues ; ///< minimal value of the mesh in each direction
    Eigen::ArrayXd m_step; ///< Step in each direction
    Eigen::ArrayXi m_nbStep ; ///< Number of steps in each dimension
    int m_nbMeshTotal ; ///< Total number of meshes
    std::vector< Eigen::Array2i > m_simAndCell; ///< For element if gives the active simulation and the cell it belongs to
    Eigen::ArrayXi  m_simToCell; ///< if each simulation give it cell number of -1 if the simulation doesn't belong to any cell


    friend class boost::serialization::access ;

    /// \brief boost serialization
    /// \param p_ar archive used for serialization
    template< typename Archive>
    void serialize(Archive &p_ar ,  unsigned int)
    {
        p_ar   &m_lowValues;
        p_ar   &m_step ;
        p_ar   &m_nbStep  ;
        p_ar &m_nbMeshTotal;
        p_ar   &m_simAndCell;
        p_ar   &m_simToCell;
    }

    /// \brief fill in arrays m_simAndCell, m_simToCell
    /// \param  p_particles    particles used for the meshes.
    ///                        First dimension  : dimension of the problem,
    void fillInSimCell(const Eigen::ArrayXXd  &p_particles)
    {
        if (p_particles.cols() != m_simToCell.size())
            m_simToCell.resize(p_particles.cols());
        m_simAndCell.clear();
        m_simAndCell.reserve(p_particles.cols());
        // utilitary
        Eigen::ArrayXi idec(p_particles.rows());
        idec(0) = 1.;
        for (int id = 1; id < p_particles.rows(); ++id)
            idec(id) = idec(id - 1) * m_nbStep(id - 1);
        Eigen::ArrayXi iposition(p_particles.rows());
        for (int is = 0; is < p_particles.cols(); ++is)
        {
            bool bAdd = true;
            for (int id = 0; id < iposition.size(); ++id)
            {
                double position = (p_particles(id, is) - m_lowValues(id)) / m_step(id);
                if ((position < m_nbStep(id)) && (position >= 0.))
                    iposition(id) = position ;
                else
                {
                    bAdd = false ;
                    break;
                }
            }
            if (bAdd)
            {
                int ipos = 0 ;
                for (int id = 0; id < iposition.size(); ++id)
                    ipos += iposition(id) * idec(id);
                m_simAndCell.push_back(Eigen::Array2i(is, ipos));
                m_simToCell(is) = ipos;
            }
            else
                m_simToCell(is) = -1;
        }
    }

    /// \brief find point location
    /// \param p_coordinates  point coordinates
    /// \return cell where the point below to or -1
    inline int pointLocation(const Eigen::ArrayXd &p_coordinates) const
    {
        int iret = 0;
        int idec = 1 ;
        for (int id = 0; id < p_coordinates.size(); ++id)
        {
            double position = (p_coordinates(id) - m_lowValues(id)) / m_step(id);
            if ((position < m_nbStep(id)) && (position >= 0.))
                iret += static_cast<int>(position) * idec;
            else
                return -1;
            idec *= m_nbStep(id);
        }
        return iret;
    }


public :

    /// \brief Default ructor
    LocalSameSizeRegression() {}

    /// \brief Constructor
    /// \param p_lowValues  in each dimension minimal value of the grid
    /// \param p_step       in each dimension the step size
    /// \param p_nbStep     in each dimension the number of steps
    LocalSameSizeRegression(const Eigen::ArrayXd &p_lowValues, const Eigen::ArrayXd &p_step, const  Eigen::ArrayXi &p_nbStep):
        m_lowValues(p_lowValues), m_step(p_step), m_nbStep(p_nbStep), m_nbMeshTotal(p_nbStep.prod()) {};

    /// \brief Constructor for object constructed at each time step
    /// \param  p_bZeroDate    first date is 0?
    /// \param  p_particles    particles used for the meshes.
    ///                        First dimension  : dimension of the problem,
    ///                        second dimension : the  number of particles
    /// \param p_lowValues  in each dimension minimal value of the grid
    /// \param p_step       in each dimension the step size
    /// \param p_nbStep     in each dimension the number of steps
    LocalSameSizeRegression(const bool &p_bZeroDate,
                            const std::shared_ptr< Eigen::ArrayXXd > &p_particles,
                            const Eigen::ArrayXd &p_lowValues,
                            const Eigen::ArrayXd &p_step,
                            const Eigen::ArrayXi &p_nbStep):
        BaseRegression(p_bZeroDate, p_particles),  m_lowValues(p_lowValues), m_step(p_step), m_nbStep(p_nbStep), m_nbMeshTotal(p_nbStep.prod()),
        m_simToCell(p_particles->cols())
    {
        if (!m_bZeroDate)
        {
            fillInSimCell(*p_particles);
        }
    }


    /// \brief Constructor for object constructed at each time step
    /// \param  p_bZeroDate    first date is 0?
    /// \param p_lowValues  in each dimension minimal value of the grid
    /// \param p_step       in each dimension the step size
    /// \param p_nbStep     in each dimension the number of steps
    LocalSameSizeRegression(const bool &p_bZeroDate,
                            const Eigen::ArrayXd &p_lowValues,
                            const Eigen::ArrayXd &p_step,
                            const Eigen::ArrayXi &p_nbStep):
        BaseRegression(p_bZeroDate),  m_lowValues(p_lowValues), m_step(p_step), m_nbStep(p_nbStep), m_nbMeshTotal(p_nbStep.prod())
    {
    }

    /// \brief Copy constructor
    /// \param p_object object to copy
    LocalSameSizeRegression(const LocalSameSizeRegression   &p_object) : BaseRegression(p_object),
        m_lowValues(p_object.getLowValues()), m_step(p_object.getStep()), m_nbStep(p_object.getNbStep()), m_nbMeshTotal(p_object.getNbMeshTotal()),
        m_simAndCell(p_object.getSimAndCell()), m_simToCell(p_object.getSimToCell())
    {}


    /// \name get back value
    ///@{
    const Eigen::ArrayXd &getLowValues() const
    {
        return  m_lowValues ;
    }
    const Eigen::ArrayXd &getStep() const
    {
        return  m_step ;
    }

    const Eigen::ArrayXi &getNbStep() const
    {
        return m_nbStep;
    }
    ///@}

    /// \brief get number of steps in one direction
    /// \param p_id dimension concerned
    inline int getNbStep(const int &p_id) const
    {
        return m_nbStep(p_id);
    }
    ///@}


    /// \brief get back the simulations belong to the cells and the cell numbers
    inline const std::vector< Eigen::Array2i >   &getSimAndCell() const
    {
        return m_simAndCell ;
    };

    /// \brief get back  for each simulation its cell number
    inline const Eigen::ArrayXi &getSimToCell() const
    {
        return m_simToCell ;
    };

    /// \brief get the total number of meshes
    inline int getNbMeshTotal() const
    {
        return m_nbMeshTotal;
    }

    /// \brief get the total number of cells
    inline int getNumberOfCells() const
    {
        if (m_nbStep.size() > 0)
            return m_nbMeshTotal;
        return 1;
    };
};
/**@}*/
}
#endif /*LOCALSAMESIZEREGRESSION_H*/
