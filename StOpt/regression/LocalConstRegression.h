// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef LOCALCONSTREGRESSION_H
#define LOCALCONSTREGRESSION_H
#include <vector>
#include <memory>
#include <array>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <Eigen/Dense>
#include "StOpt/regression/LocalRegression.h"
#include "StOpt/core/grids/InterpolatorSpectral.h"

/** \file   LocalConstRegression.h
 *  \brief  Compute conditional expectations by using constant local regressions.
 *          See article "Monte-Carlo valorisation of American options: facts and new algorithms to improve existing methods"
 *          for the linear approximation by Bouchard, Warin in "Numerical methods in finance", Springer,2012
 *  \author Xavier Warin
 */
namespace StOpt
{
/**
 * \defgroup LocalConst Piecewise constant  regressions
 * \brief It implements local constant functions for performing local regressions
 *@{
 */
/// \class LocalConstRegression LocalConstRegression.h
/// To be used in Monte Carlo methods. Constant regression on each cell which is constructed such that each cell has
/// roughly the same number of particles
class LocalConstRegression : public LocalRegression
{
protected :

    Eigen::ArrayXd m_matReg    ;            ///< Regression diagonal matrix (rank the number of meshes ).


public :

    /// \brief Default constructor
    LocalConstRegression() {}

    /// \brief Constructor
    /// \param  p_nbMesh       discretization in each direction
    LocalConstRegression(const Eigen::ArrayXi   &p_nbMesh);

    /// \brief Constructor for object constructed at each time step
    /// \param  p_bZeroDate    first date is 0?
    /// \param  p_particles    particles used for the meshes.
    ///                        First dimension  : dimension of the problem,
    ///                        second dimension : the  number of particles
    /// \param  p_nbMesh       discretization in each direction
    LocalConstRegression(const bool &p_bZeroDate,
                         const std::shared_ptr< Eigen::ArrayXXd > &p_particles,
                         const Eigen::ArrayXi   &p_nbMesh);

    /// \brief Second constructor , only to be used in simulation
    LocalConstRegression(const bool &p_bZeroDate, const std::vector< std::shared_ptr< Eigen::ArrayXd > > &p_mesh1D) : LocalRegression(p_bZeroDate, p_mesh1D) {}

    /// \brief Copy constructor
    /// \param p_objet object to copy
    LocalConstRegression(const LocalConstRegression   &p_objet);


    /// \brief update the particles used in regression  and construct the matrices
    /// \param  p_bZeroDate    first date is 0?
    /// \param  p_particles    particles used for the meshes.
    ///                        First dimension  : dimension of the problem,
    ///                        second dimension : the  number of particles
    void updateSimulations(const bool &p_bZeroDate, const std::shared_ptr< Eigen::ArrayXXd> &p_particles);

    /// \brief Get some local accessors
    ///@{
    inline const Eigen::ArrayXd &getMatReg() const
    {
        return m_matReg;
    }
    ///@}
    /// \brief conditional expectation basis function coefficient calculation
    /// \param  p_fToRegress  function to regress associated to each simulation used in optimization
    /// \return regression coordinates on the basis  (size : number of meshes multiplied by the dimension plus one)
    /// @{
    Eigen::ArrayXd getCoordBasisFunction(const Eigen::ArrayXd &p_fToRegress) const;
    Eigen::ArrayXXd getCoordBasisFunctionMultiple(const Eigen::ArrayXXd &p_fToRegress) const ;
    ///@}

    /// \brief conditional expectation calculation
    /// \param  p_fToRegress  simulations  to regress used in optimization
    /// \return regressed value function
    /// @{
    Eigen::ArrayXd getAllSimulations(const Eigen::ArrayXd &p_fToRegress) const ;
    Eigen::ArrayXXd getAllSimulationsMultiple(const Eigen::ArrayXXd &p_fToRegress) const;
    ///@}

    /// \brief Use basis functions to reconstruct the solution
    /// \param p_basisCoefficients basis coefficients
    ///@{
    Eigen::ArrayXd reconstruction(const Eigen::ArrayXd   &p_basisCoefficients) const;
    Eigen::ArrayXXd reconstructionMultiple(const Eigen::ArrayXXd   &p_basisCoefficients) const;
    /// @}
    /// \brief use basis function to reconstruct a given simulation
    /// \param p_isim               simulation number
    /// \param p_basisCoefficients  basis coefficients to reconstruct a given conditional expectation
    double reconstructionASim(const int &p_isim , const Eigen::ArrayXd   &p_basisCoefficients) const ;

    /// \brief conditional expectation reconstruction
    /// \param  p_coordinates        coordinates to interpolate (uncertainty sample)
    /// \param  p_coordBasisFunction regression coordinates on the basis  (size: number of meshes multiplied by the dimension plus one)
    /// \return regressed value function reconstructed for each simulation
    double getValue(const Eigen::ArrayXd   &p_coordinates,
                    const Eigen::ArrayXd   &p_coordBasisFunction) const;


    /// \brief permits to reconstruct a function with basis functions coefficients values given on a grid
    /// \param  p_coordinates            coordinates  (uncertainty sample)
    /// \param  p_ptOfStock              grid point
    /// \param  p_interpFuncBasis        spectral interpolator to interpolate the basis functions  coefficients used in regression on the grid (given for each basis function)
    double getAValue(const Eigen::ArrayXd &p_coordinates,  const Eigen::ArrayXd &p_ptOfStock,
                     const std::vector< std::shared_ptr<InterpolatorSpectral> > &p_interpFuncBasis) const;


    /// \brief get the number of basis functions
    inline int getNumberOfFunction() const
    {
        if (m_bZeroDate)
            return 1;
        else
            return m_nbMesh.prod() ;
    }

    /// \brief Clone the regressor
    std::shared_ptr<BaseRegression> clone() const
    {
        return std::static_pointer_cast<BaseRegression>(std::make_shared<LocalConstRegression>(*this));
    }


};
/**@}*/
}
#endif /*LOCALCONSTREGRESSION_H*/
