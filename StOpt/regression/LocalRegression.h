// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef LOCALREGRESSION_H
#define LOCALREGRESSION_H
#include <vector>
#include <memory>
#include <array>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <Eigen/Dense>
#include "StOpt/regression/BaseRegression.h"
#include "StOpt/regression/meshCalculationLocalRegression.h"
#include "StOpt/core/grids/InterpolatorSpectral.h"

/** \file   LocalRegression.h
 *  \brief  Base class for local regressions with adapted mesh size.
 *          See for the linear version the  article "Monte-Carlo valorisation of American options: facts and new algorithms to improve existing methods"
 *          by Bouchard, Warin in "Numerical methods in finance", Springer,2012
 *  \author Xavier Warin
 */
namespace StOpt
{
/**
 * \defgroup Local Piecewise regression
 * \brief It implements local local regression
  *@{
 */
/// \class LocalRegression LocalRegression.h
/// To be used in Monte Carlo methods  regression on each cell which is constructed such that each cell has
/// roughly the same number of particles
class LocalRegression : public BaseRegression
{
protected :

    Eigen::ArrayXi  m_nbMesh ;    ///< Number of discretization meshes in each direction: copy because of python binding
    int m_nbMeshTotal ; ///< Total number of meshes
    Eigen::Array< std::array< double, 2> , Eigen::Dynamic, Eigen::Dynamic > m_mesh ; ///< In each dimension, for each mesh, give the coordinates min and max of the mesh.
    std::vector< std::shared_ptr <Eigen::ArrayXd>  > m_mesh1D ; ///< Mesh representation per dimension
    Eigen::ArrayXi m_simToCell;              ///< For each simulation gives its global mesh number

    friend class boost::serialization::access ;

    /// \brief boost serialization
    /// \param p_ar archive used for serialization
    template< typename Archive>
    void serialize(Archive &p_ar ,  unsigned int)
    {
        p_ar &m_bZeroDate;
        p_ar &m_mesh1D ;
    }


public :

    /// \brief Default ructor
    LocalRegression() {}

    /// \brief Constructor
    /// \param  p_nbMesh       discretization in each direction
    LocalRegression(const Eigen::ArrayXi   &p_nbMesh): m_nbMesh(p_nbMesh), m_nbMeshTotal(m_nbMesh.prod()) {};

    /// \brief Constructor for object constructed at each time step
    /// \param  p_bZeroDate    first date is 0?
    /// \param  p_particles    particles used for the meshes.
    ///                        First dimension  : dimension of the problem,
    ///                        second dimension : the  number of particles
    /// \param  p_nbMesh       discretization in each direction
    LocalRegression(const bool &p_bZeroDate,
                    const std::shared_ptr< Eigen::ArrayXXd > &p_particles,
                    const Eigen::ArrayXi   &p_nbMesh):
        BaseRegression(p_bZeroDate, p_particles), m_nbMesh(p_nbMesh),
        m_nbMeshTotal(m_nbMesh.prod()), m_simToCell(p_particles->cols())
    {
        if ((!m_bZeroDate) && (p_nbMesh.size() != 0))
        {
            meshCalculationLocalRegression(*m_particles, m_nbMesh , m_simToCell, m_mesh, m_mesh1D);
        }
        else
        {
            m_simToCell.setConstant(0);
            m_nbMeshTotal = 1;
        }
    }

    /// \brief Second constructor , only to be used in simulation
    LocalRegression(const bool &p_bZeroDate, const std::vector< std::shared_ptr< Eigen::ArrayXd > > &p_mesh1D) : BaseRegression(p_bZeroDate), m_nbMesh(p_mesh1D.size()), m_mesh1D(p_mesh1D)
    {
        if (!m_bZeroDate)
        {
            for (size_t i = 0; i < p_mesh1D.size(); ++i)
                m_nbMesh(i) = p_mesh1D[i]->size() - 1;
            m_nbMeshTotal = m_nbMesh.prod();
        }
        else
            m_nbMeshTotal = 1;
    }

    /// \brief Copy constructor
    /// \param p_object object to copy
    LocalRegression(const LocalRegression   &p_object) : BaseRegression(p_object), m_nbMesh(p_object.getNbMesh()),
        m_nbMeshTotal(p_object.getNbMeshTotal()), m_mesh(p_object.getMesh()),
        m_simToCell(p_object.getSimToCell())
    {
        const std::vector< std::shared_ptr< Eigen::ArrayXd > > &mesh1D = p_object.getMesh1D();
        m_mesh1D.resize(mesh1D.size());
        for (size_t i = 0 ; i < m_mesh1D.size(); ++i)
            m_mesh1D[i] = std::make_shared< Eigen::ArrayXd>(*mesh1D[i]);
    }

    /// \brief Get some local accessors
    ///@{
    inline const Eigen::ArrayXi getNbMesh() const
    {
        return m_nbMesh;
    }
    inline const Eigen::Array< std::array< double, 2> , Eigen::Dynamic, Eigen::Dynamic > &getMesh() const
    {
        return m_mesh;
    }
    inline const std::vector< std::shared_ptr< Eigen::ArrayXd > > &getMesh1D() const
    {
        return m_mesh1D;
    }
    inline const Eigen::ArrayXi &getSimToCell() const
    {
        return m_simToCell;
    }
    ///@}


    /// \brief get the total number of meshes
    inline int getNbMeshTotal() const
    {
        return m_nbMeshTotal;
    }

    /// \brief get the total number of cells
    inline int getNumberOfCells() const
    {
        if (m_nbMesh.size() > 0)
            return m_nbMesh.prod();
        return 1;
    };
};
/**@}*/
}
#endif /*LOCALREGRESSION_H*/
