// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef BASEREGRESSION_H
#define BASEREGRESSION_H
#include <memory>
#include <vector>
#include <Eigen/Dense>
#include "StOpt/core/grids/InterpolatorSpectral.h"

/** \file BaseRegression.h
 *  \brief Base class to define regressor for stochastic optimization by Monte Carlo
 *  \author Xavier Warin
 */
namespace StOpt
{
/// \class BaseRegression BaseRegression.h
/// Base class for regression
class BaseRegression
{
protected :

    bool m_bZeroDate ;                    ///< Is the regression date zero ?
    std::shared_ptr<Eigen::ArrayXXd>  m_particles;  ///< Particles used to regress: first dimension  : dimension of the problem , second dimension : the  number of particles

public :

    /// \brief Default constructor
    BaseRegression() {}

    /// \brief Default destructor
    virtual ~BaseRegression() {}

    /// \brief Constructor storing the particles
    /// \param  p_bZeroDate    first date is 0?
    /// \param  p_particles    particles used for the meshes.
    ///                        First dimension  : dimension of the problem,
    ///                        second dimension : the  number of particles
    BaseRegression(const bool &p_bZeroDate, const std::shared_ptr< Eigen::ArrayXXd> &p_particles) : m_bZeroDate(p_bZeroDate), m_particles(p_particles) {}


    /// \brief Last constructor used in simulation
    /// \param  p_bZeroDate    first date is 0?
    BaseRegression(const bool &p_bZeroDate) : m_bZeroDate(p_bZeroDate) {}

    /// \brief Copy constructor
    /// \param p_object  object to copy
    BaseRegression(const BaseRegression &p_object): m_bZeroDate(p_object.getBZeroDate()), m_particles(p_object.getParticles()) {}

    /// \brief update the particles used in regression  and construct the matrices
    /// \param  p_bZeroDate    first date is 0?
    /// \param  p_particles    particles used for the meshes.
    ///                        First dimension  : dimension of the problem,
    ///                        second dimension : the  number of particles
    void updateSimulationsBase(const bool &p_bZeroDate, const std::shared_ptr< Eigen::ArrayXXd> &p_particles)
    {
        m_bZeroDate = p_bZeroDate;
        m_particles = p_particles;

    }

    /// \brief Get some local accessors
    ///@{
    inline std::shared_ptr< Eigen::ArrayXXd > getParticles() const
    {
        return m_particles ;
    }

    /// \brief Get the object by reference
    inline const Eigen::ArrayXXd &getParticlesRef() const
    {
        return *m_particles;
    }

    /// \brief Get dimension of the problem
    inline int getDimension() const
    {
        return m_particles->rows();
    }

    /// \brief Get the number of simulations
    inline int  getNbSimul()const
    {
        return m_particles->cols() ;
    }

    /// \brief get the number of basis functions
    virtual  int getNumberOfFunction() const = 0 ;

    ///@}
    /// \brief Constructor storing the particles
    /// \brief update the particles used in regression  and construct the matrices
    /// \param  p_bZeroDate    first date is 0?
    /// \param  p_particles    particles used for the meshes.
    ///                        First dimension  : dimension of the problem,
    ///                        second dimension : the  number of particles
    virtual void updateSimulations(const bool &p_bZeroDate, const std::shared_ptr< Eigen::ArrayXXd> &p_particles) = 0 ;

    /// \brief conditional expectation basis function coefficient calculation
    /// \param  p_fToRegress  function to regress associated to each simulation used in optimization
    /// \return regression coordinates on the basis  (size : number of meshes multiplied by the dimension plus one)
    /// @{
    virtual Eigen::ArrayXd getCoordBasisFunction(const Eigen::ArrayXd &p_fToRegress) const = 0;
    virtual Eigen::ArrayXXd getCoordBasisFunctionMultiple(const Eigen::ArrayXXd &p_fToRegress) const = 0 ;
    ///@}

    /// \brief conditional expectation calculation
    /// \param  p_fToRegress  simulations  to regress used in optimization
    /// \return regressed value function
    /// @{
    virtual Eigen::ArrayXd getAllSimulations(const Eigen::ArrayXd &p_fToRegress) const = 0;
    virtual Eigen::ArrayXXd getAllSimulationsMultiple(const Eigen::ArrayXXd &p_fToRegress) const = 0;
    ///@}

    /// \brief Use basis functions to reconstruct the solution
    /// \param p_basisCoefficients basis coefficients
    ///@{
    virtual Eigen::ArrayXd reconstruction(const Eigen::ArrayXd   &p_basisCoefficients) const = 0 ;
    virtual Eigen::ArrayXXd reconstructionMultiple(const Eigen::ArrayXXd   &p_basisCoefficients) const = 0;
    /// @}

    /// \brief use basis function to reconstruct a given simulation
    /// \param p_isim               simulation number
    /// \param p_basisCoefficients  basis coefficients to reconstruct a given conditional expectation
    virtual double reconstructionASim(const int &p_isim , const Eigen::ArrayXd   &p_basisCoefficients) const = 0 ;

    /// \brief conditional expectation reconstruction
    /// \param  p_coordinates        coordinates to interpolate (uncertainty sample)
    /// \param  p_coordBasisFunction regression coordinates on the basis  (size: number of meshes multiplied by the dimension plus one)
    /// \return regressed value function reconstructed for each simulation
    virtual double getValue(const Eigen::ArrayXd   &p_coordinates,
                            const Eigen::ArrayXd   &p_coordBasisFunction) const = 0;

    /// \brief permits to reconstruct a function with basis functions coefficients values given on a grid
    /// \param  p_coordinates          coordinates  (uncertainty sample)
    /// \param  p_ptOfStock            grid point
    /// \param  p_interpFuncBasis      spectral interpolator to interpolate the basis functions  coefficients used in regression on the grid (given for each basis function)
    virtual double getAValue(const Eigen::ArrayXd &p_coordinates,  const Eigen::ArrayXd &p_ptOfStock,
                             const std::vector< std::shared_ptr<InterpolatorSpectral> > &p_interpFuncBasis) const = 0;

    /// \brief is the regression date zero
    inline bool getBZeroDate() const
    {
        return m_bZeroDate;
    }

    /// \brief Clone the regressor
    virtual std::shared_ptr<BaseRegression> clone() const = 0 ;

};

}

#endif
