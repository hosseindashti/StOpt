// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef LOCALSAMESIZELINEARREGRESSIONGENERS_H
#define LOCALSAMESIZELINEARREGRESSIONGENERS_H
#include <geners/AbsReaderWriter.hh>
#include "StOpt/regression/LocalSameSizeLinearRegression.h"
#include "StOpt/regression/BaseRegressionGeners.h"

/** \file LocalSameSizeLinearRegressionGeners.h
 * \brief Define non intrusive  serialization with random access
*  \author Xavier Warin
 */

// Concrete reader/writer for class LocalSameSizeLinearRegression
// Note publication of LocalSameSizeLinearRegression as "wrapped_type".
struct LocalSameSizeLinearRegressionGeners: public gs::AbsReaderWriter<StOpt::BaseRegression>
{
    typedef StOpt::BaseRegression wrapped_base;
    typedef StOpt::LocalSameSizeLinearRegression wrapped_type;

    // Methods that have to be overridden from the base
    bool write(std::ostream &, const wrapped_base &, bool p_dumpId) const override;
    wrapped_type *read(const gs::ClassId &p_id, std::istream &p_in) const override;

    // The class id forLocalSameSizeLinearRegression  will be needed both in the "read" and "write"
    // methods. Because of this, we will just return it from one static
    // function.
    static const gs::ClassId &wrappedClassId();
};

gs_specialize_class_id(StOpt::LocalSameSizeLinearRegression , 1)
gs_declare_type_external(StOpt::LocalSameSizeLinearRegression)
gs_associate_serialization_factory(StOpt::LocalSameSizeLinearRegression, StaticSerializationFactoryForBaseRegression)

#endif  /* LOCALSAMESIZELINEARREGRESSIONGENERS_H */
