// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef LOCALSAMESIZECONSTREGRESSION_H
#define LOCALSAMESIZECONSTREGRESSION_H
#include <vector>
#include <memory>
#include <array>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <Eigen/Dense>
#include "StOpt/regression/LocalSameSizeRegression.h"
#include "StOpt/core/grids/InterpolatorSpectral.h"

/** \file   LocalSameSizeConstRegression.h
 *  \brief  Compute conditional expectations by using constant local regressions with fixed size meshes.
 *  \author Xavier Warin
 */
namespace StOpt
{
/**
 * \defgroup LocalConstSameSize Piecewise constant  regressions with constant mesh size
 * \brief It implements local constant functions for performing local regressions with constant meshes
 *@{
 */
/// \class LocalSameSizeConstRegression LocalSameSizeConstRegression.h
/// Constant regression on each cell
class LocalSameSizeConstRegression : public LocalSameSizeRegression
{
protected :

    Eigen::ArrayXd m_matReg    ;            ///< Regression diagonal matrix (rank the number of meshes ).

public :

    /// \brief Default constructor
    LocalSameSizeConstRegression() {}

    /// \brief Constructor
    /// \param p_lowValues  in each dimension minimal value of the grid
    /// \param p_step       in each dimension the step size
    /// \param p_nbStep     in each dimension the number of steps
    LocalSameSizeConstRegression(const Eigen::ArrayXd &p_lowValues, const Eigen::ArrayXd &p_step, const  Eigen::ArrayXi &p_nbStep) : LocalSameSizeRegression(p_lowValues, p_step, p_nbStep) {} ;

    /// \brief Constructor for object constructed at each time step
    /// \param  p_bZeroDate    first date is 0?
    /// \param  p_particles    particles used for the meshes.
    ///                        First dimension  : dimension of the problem,
    ///                        second dimension : the  number of particles
    /// \param p_lowValues     in each dimension minimal value of the grid
    /// \param p_step          in each dimension the step size
    /// \param p_nbStep        in each dimension the number of steps
    LocalSameSizeConstRegression(const bool &p_bZeroDate,
                                 const std::shared_ptr< Eigen::ArrayXXd > &p_particles,
                                 const Eigen::ArrayXd &p_lowValues,
                                 const Eigen::ArrayXd &p_step,
                                 const  Eigen::ArrayXi &p_nbStep);


    /// \brief Constructor for object constructed at each time step
    /// \param  p_bZeroDate    first date is 0?
    /// \param p_lowValues  in each dimension minimal value of the grid
    /// \param p_step       in each dimension the step size
    /// \param p_nbStep     in each dimension the number of steps
    LocalSameSizeConstRegression(const bool &p_bZeroDate,
                                 const Eigen::ArrayXd &p_lowValues,
                                 const Eigen::ArrayXd &p_step,
                                 const Eigen::ArrayXi &p_nbStep):
        LocalSameSizeRegression(p_bZeroDate, p_lowValues, p_step, p_nbStep) {}

    /// \brief Copy constructor
    /// \param p_objet object to copy
    LocalSameSizeConstRegression(const LocalSameSizeConstRegression   &p_objet);


    /// \brief update the particles used in regression  and construct the matrices
    /// \param  p_bZeroDate    first date is 0?
    /// \param  p_particles    particles used for the meshes.
    ///                        First dimension  : dimension of the problem,
    ///                        second dimension : the  number of particles
    void updateSimulations(const bool &p_bZeroDate, const std::shared_ptr< Eigen::ArrayXXd> &p_particles);

    /// \brief Get some local accessors
    ///@{
    inline const Eigen::ArrayXd &getMatReg() const
    {
        return m_matReg;
    }
    ///@}
    /// \brief conditional expectation basis function coefficient calculation
    /// \param  p_fToRegress  function to regress associated to each simulation used in optimization
    /// \return regression coordinates on the basis  (size : number of meshes multiplied by the dimension plus one)
    /// @{
    Eigen::ArrayXd getCoordBasisFunction(const Eigen::ArrayXd &p_fToRegress) const;
    Eigen::ArrayXXd getCoordBasisFunctionMultiple(const Eigen::ArrayXXd &p_fToRegress) const ;
    ///@}

    /// \brief conditional expectation calculation
    /// \param  p_fToRegress  simulations  to regress used in optimization
    /// \return regressed value function
    /// @{
    Eigen::ArrayXd getAllSimulations(const Eigen::ArrayXd &p_fToRegress) const ;
    Eigen::ArrayXXd getAllSimulationsMultiple(const Eigen::ArrayXXd &p_fToRegress) const;
    ///@}

    /// \brief Use basis functions to reconstruct the solution
    /// \param p_basisCoefficients basis coefficients
    ///@{
    Eigen::ArrayXd reconstruction(const Eigen::ArrayXd   &p_basisCoefficients) const;
    Eigen::ArrayXXd reconstructionMultiple(const Eigen::ArrayXXd   &p_basisCoefficients) const;
    /// @}
    /// \brief use basis function to reconstruct a given simulation
    /// \param p_isim               simulation number
    /// \param p_basisCoefficients  basis coefficients to reconstruct a given conditional expectation
    double reconstructionASim(const int &p_isim , const Eigen::ArrayXd   &p_basisCoefficients) const ;

    /// \brief conditional expectation reconstruction
    /// \param  p_coordinates        coordinates to interpolate (uncertainty sample)
    /// \param  p_coordBasisFunction regression coordinates on the basis  (size: number of meshes multiplied by the dimension plus one)
    /// \return regressed value function reconstructed for each simulation
    double getValue(const Eigen::ArrayXd   &p_coordinates,
                    const Eigen::ArrayXd   &p_coordBasisFunction) const;


    /// \brief permits to reconstruct a function with basis functions coefficients values given on a grid
    /// \param  p_coordinates            coordinates  (uncertainty sample)
    /// \param  p_ptOfStock              grid point
    /// \param  p_interpFuncBasis        spectral interpolator to interpolate the basis functions  coefficients used in regression on the grid (given for each basis function)
    double getAValue(const Eigen::ArrayXd &p_coordinates,  const Eigen::ArrayXd &p_ptOfStock,
                     const std::vector< std::shared_ptr<InterpolatorSpectral> > &p_interpFuncBasis) const;


    /// \brief get the number of basis functions
    inline int getNumberOfFunction() const
    {
        if (m_bZeroDate)
            return 1;
        else
            return m_nbMeshTotal ;
    }

    /// \brief Clone the regressor
    std::shared_ptr<BaseRegression> clone() const
    {
        return std::static_pointer_cast<BaseRegression>(std::make_shared<LocalSameSizeConstRegression>(*this));
    }
};
/**@}*/
}
#endif /*LOCALSAMESIZECONSTREGRESSION_H*/
