// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef VERSION_H
#define VERSION_H
#include <string>
/// Major code version
#define STOPT_MAJOR_VERSION 1
/// Minor code version
#define STOPT_MINOR_VERSION 0

/** \file version.h
 * \brief Defines StOpt version
 * \author Xavier Warin
 */

namespace StOpt
{
/// \brief get back library version
std::string getStOptVersion();
}
#endif
