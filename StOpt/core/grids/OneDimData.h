// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef ONEDIMDATA_H
#define ONEDIMDATA_H
#include <memory>
#include <Eigen/Dense>
#include <iostream>
#include <vector>

/** \file OneDimData.h
 * \brief Defines a Data specified on a One Dimensional Grid with a constant par step value
 * \author Xavier Warin
 */
namespace StOpt
{
/// \class OneDimData OneDimData.h
/// Some data being constant per mesh in one dimensional setting
template< class OneDimGrid, typename T  >
class OneDimData
{
private :
    std::shared_ptr< OneDimGrid> m_grid ; ///< One dimensional grid
    std::shared_ptr<std::vector<T> > m_values ; ///< values associated to the grid

public :
    ///\brief Constructor
    /// \param p_grid  One dimensional grid
    /// \param p_values values on the grid
    OneDimData(const std::shared_ptr< OneDimGrid>   &p_grid, const std::shared_ptr<std::vector<T> > &p_values): m_grid(p_grid), m_values(p_values)
    {
        assert(p_values->size() ==  p_grid->getNbStep() + 1);
    }
    /// \brief get the value interpolated constant per mesh at a given point
    /// \param p_coord   the abscissa
    /// \return interpolated value
    inline T  get(const double &p_coord) const
    {
        return (*m_values)[m_grid->getMesh(p_coord)];
    }
};
}
#endif
