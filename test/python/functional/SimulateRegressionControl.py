# Copyright (C) 2016 EDF
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
import numpy as np
import StOptReg as reg
import StOptGrids
import StOptGlobal


# Simulate the optimal strategy , threaded version
# p_grid                   grid used for  deterministic state (stocks for example)
# p_optimize               optimizer defining the optimization between two time steps
# p_funcFinalValue         function defining the final value
# p_pointStock             initial point stock
# p_initialRegime          regime at initial date
# p_fileToDump             name of the file used to dump continuation values in optimization
def SimulateRegressionControl(p_grid, p_optimize, p_funcFinalValue, p_pointStock, p_initialRegime, p_fileToDump) :
    
    simulator = p_optimize.getSimulator()
    nbStep = simulator.getNbStep()
    states = []
    
    for i in range(simulator.getNbSimul()) :
        states.append(StOptGlobal.StateWithStocks(p_initialRegime, p_pointStock, np.zeros(simulator.getDimension())))
            
    ar = StOptGlobal.BinaryFileArchive(p_fileToDump, "r")
    # name for continuation object in archive
    nameAr = "Continuation"
    # cost function
    costFunction = np.zeros((p_optimize.getSimuFuncSize(), simulator.getNbSimul()))
    
    # iterate on time steps
    for istep in range(nbStep) :
        NewState = StOptGlobal.SimulateStepRegressionControl(ar, nbStep - 1 - istep, nameAr, p_grid, p_optimize).oneStep(states, costFunction)
        # different from C++
        states = NewState[0]
        costFunction = NewState[1]
        # new stochastic state
        particles = simulator.stepForwardAndGetParticles()
        
        for i in range(simulator.getNbSimul()) :
            states[i].setStochasticRealization(particles[:,i])
            
    # final : accept to exercise if not already done entirely
    for i in range(simulator.getNbSimul()) :
        costFunction[0,i] += p_funcFinalValue.set(states[i].getRegime(), states[i].getPtStock(), states[i].getStochasticRealization()) * simulator.getActu()
        
    # average gain/cost
    return costFunction.mean()
