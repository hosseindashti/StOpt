# Copyright (C) 2016 EDF
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
import numpy as np
import StOptGlobal
import math


# Basket payoff (until get full mapping)
def almostEqual(x, y , ulp):
    
    return abs(x - y) <= 2.220446049250313e-16 * abs(x + y) * ulp # 2.220446049250313e-16 = std::numeric_limits.epsilon() in C++


    
# Simulate the optimal strategy , threaded version
# p_grid                   grid used for  deterministic state (stocks for example)
# p_optimize               optimizer defining the optimization between two time steps
# p_payOff                 function defining the final value (pay off)
# p_pointStock             initial point stock
# p_initialRegime          regime at initial date
# p_fileToDump             name of the file used to dump continuation values in optimization
def SimulateRegressionHighLevel(p_grid, p_optimize, p_payOff , p_pointStock, p_initialRegime, p_fileToDump) :
    
    # from the optimizer get back the simulation
    simulator = p_optimize.getSimulator()
    nbStep = simulator.getNbStep()
    states = []
    
    for i in range(simulator.getNbSimul()) :
        states.append(StOptGlobal.StateWithStocks(p_initialRegime, p_pointStock, np.zeros(simulator.getDimension())))
        
    # name for continuation object in archive
    nameAr = "Continuation"
    # cost function
    costFunction = np.zeros(simulator.getNbSimul())
    
    # iterate on time steps
    arRead = StOptGlobal.BinaryFileArchive(p_fileToDump, "r")
    
    for istep in range(nbStep) :
        NewState = StOptGlobal.SimulateStepRegression(arRead, nbStep - 1 - istep, nameAr, p_grid, p_optimize).oneStep(states, costFunction)
        # different from C++
        states = NewState[0]
        costFunction = NewState[1]
        # new stochastic state
        particules = simulator.stepForwardAndGetParticles()
        
        for i in range(simulator.getNbSimul()) :
            states[i].setStochasticRealization(particules[:,i])
            
        # update actualization
        p_optimize.incrementActuTimeStep()
        
    # final : accept to exercise if not already done entirely
    for i in range(simulator.getNbSimul()) :
        costFunction[i] +=  p_payOff.set(states[i].getRegime(), states[i].getPtStock(), states[i].getStochasticRealization()) * p_optimize.getActuStep()
        
    # average gain/cost
    return costFunction.mean()
