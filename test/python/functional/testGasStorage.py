# Copyright (C) 2016 EDF
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
import numpy as np
import math
import OneDimRegularSpaceGrid as rsg
import OneDimData as data
import MeanRevertingSimulator as mrsim
import OptimizeGasStorage as ogs
import DynamicProgrammingByRegression as dyn
import StOptReg as reg
import StOptGrids
import unittest

accuracyClose = 1.5


class ZeroFunction :
    
    def __init__(self) :
        
        return None

    def set(self, a, b, c) :
        
        return 0.

# valorization of a given gas storage on a  grid
# p_grid             the grid
# p_maxLevelStorage  maximum level
def testGasStorage(p_grid, p_maxLevelStorage) :
    
    # storage
    injectionRateStorage = 60000
    withdrawalRateStorage = 45000
    injectionCostStorage = 0.35
    withdrawalCostStorage = 0.35

    maturity = 1.
    nstep = 100

    # define a a time grid
    timeGrid = rsg.OneDimRegularSpaceGrid(np.zeros(1), maturity / nstep, nstep)
    # future values
    futValues = np.zeros(nstep + 1)
    
    # periodicity factor
    iPeriod = 52
    
    for i in list(range(nstep + 1)) :
        futValues[i] = 50. + 20 * math.sin((math.pi * i * iPeriod) / nstep)
        
    # define the future curve
    futureGrid = data.OneDimData(timeGrid, futValues)
    
    # one dimensional factors
    nDim = 1
    sigma = np.zeros(nDim) + 0.94
    mr = np.zeros(nDim) + 0.29
    # number of simulations
    nbsimulOpt = 20000
    
    # no actualization
    rate=0
    # a backward simulator
    bForward = False
    
    backSimulator = mrsim.MeanRevertingSimulator(futureGrid, sigma, mr, rate,maturity, nstep, nbsimulOpt, bForward)
    # optimizer
    storage = ogs.OptimizeGasStorage(injectionRateStorage, withdrawalRateStorage, injectionCostStorage, withdrawalCostStorage)
    
    # regressor
    nMesh = 6
    nbMesh = np.zeros(1, dtype = np.int32) + nMesh
    regressor = reg.LocalLinearRegression(nbMesh)
    # final value
    vFunction = ZeroFunction()
    
    # initial values
    initialStock = np.zeros(1) + p_maxLevelStorage
    initialRegime = 0 # only one regime
    
    # Optimize
    fileToDump = "CondExpGasStorage"
    
    # link the simulations to the optimizer
    storage.setSimulator(backSimulator)
    valueOptim = dyn.DynamicProgrammingByRegression(p_grid, storage, regressor, vFunction, initialStock, initialRegime, fileToDump)
    print("valueOptim", valueOptim)
    return valueOptim
            
class testGasStorageTest(unittest.TestCase):
          
    def test_simpleStorage(self):
              
        # storage
        #########
        maxLevelStorage = 90000
        # grid
        ######
        nGrid = 10
        lowValues = np.zeros(1)
        step = np.zeros(1) + (maxLevelStorage / nGrid)
        nbStep = np.zeros(1, dtype = np.int32) + nGrid
        grid = StOptGrids.RegularSpaceGrid(lowValues, step, nbStep)
              
        testGasStorage(grid, maxLevelStorage)
     
    def test_simpleStorageLegendreLinear(self):
         
        # storage
        #########
        maxLevelStorage = 90000
        # grid
        ######
        nGrid = 10
        lowValues = np.zeros(1)
        step = np.zeros(1) + (maxLevelStorage / nGrid)
        nbStep = np.zeros(1, dtype = np.int32) + nGrid
        poly = np.zeros(1, dtype = np.int32) + 1
        grid = StOptGrids.RegularLegendreGrid(lowValues, step, nbStep, poly)
              
        testGasStorage(grid, maxLevelStorage)
        
    def test_simpleStorageLegendreQuadratic(self):
        
        # storage
        #########
        maxLevelStorage = 90000
        # grid
        ######
        nGrid = 5
        lowValues = np.zeros(1)
        step = np.zeros(1) + (maxLevelStorage / nGrid)
        nbStep = np.zeros(1, dtype = np.int32) + nGrid
        poly = np.zeros(1, dtype = np.int32) + 2
        grid = StOptGrids.RegularLegendreGrid(lowValues, step, nbStep, poly)
              
        testGasStorage(grid, maxLevelStorage)
        
    def test_simpleStorageLegendreCubic(self):
        
        # storage
        #########
        maxLevelStorage = 90000
        # grid
        ######
        nGrid = 5
        lowValues = np.zeros(1)
        step = np.zeros(1) + (maxLevelStorage / nGrid)
        nbStep = np.zeros(1, dtype = np.int32) + nGrid
        poly = np.zeros(1, dtype = np.int32) + 3
        grid = StOptGrids.RegularLegendreGrid(lowValues, step, nbStep, poly)
              
        testGasStorage(grid, maxLevelStorage)
        
if __name__ == '__main__':
    unittest.main()
