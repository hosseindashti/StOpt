# Copyright (C) 2016 EDF
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
import numpy as np
import StateWithStocks as sws
import SimulateStepRegression as strt


# Simulate the optimal strategy , threaded version
# p_grid                   grid used for  deterministic state (stocks for example)
# p_optimize               optimizer defining the optimization between two time steps
# p_funcFinalValue         function defining the final value
# p_pointStock             initial point stock
# p_initialRegime          regime at initial date
# p_fileToDump             name of the file used to dump continuation values in optimization
def SimulateRegression(p_grid, p_optimize, p_funcFinalValue, p_pointStock, p_initialRegime, p_fileToDump) :
    
    # from the optimizer get back the simulation
    simulator = p_optimize.getSimulator()
    nbStep = simulator.getNbStep()
    states = []
    
    for i in range(simulator.getNbSimul()) :
        states.append(sws.StateWithStocks(p_initialRegime, p_pointStock, np.zeros(simulator.getDimension())))
    
    # name for continuation object in archive
    nameAr = "Continuation"
    # cost function
    costFunction = np.zeros(simulator.getNbSimul())
    
    # iterate on time steps
    for istep in range(nbStep) :
        strt.SimulateStepRegression(nbStep - 1 - istep, nameAr, p_grid, p_optimize).oneStep(states, costFunction)
        # new stochastic state
        particules = simulator.stepForwardAndGetParticles()
        
        for i in range(simulator.getNbSimul()) :
            states[i].setStochasticRealization(particules[:,i])
            
        # update actualization
        p_optimize.incrementActuTimeStep()
        
    # final : accept to exercise if not already done entirely
    for i in range(simulator.getNbSimul()) :
        costFunction[i] += p_funcFinalValue.operator(states[i].getRegime(), states[i].getPtStock(), states[i].getStochasticRealization()) * p_optimize.getActuStep()
        
    # average gain/cost
    return costFunction.mean()
