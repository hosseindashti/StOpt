# Copyright (C) 2016 EDF
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
import numpy as np
import StOptReg
import StOptGrids
import StOptGlobal


class SimulateStepRegression :
    
    def __init__(self, p_ar, p_iStep, p_nameCont, p_pGridFollowing, p_pOptimize) :
        
        self.m_pGridFollowing = p_pGridFollowing
        self.m_pOptimize = p_pOptimize
        self.m_continuationObj = np.zeros(p_pOptimize.getNbRegime())
        
        for iReg in range(self.m_pOptimize.getNbRegime()):
            bellDump = p_nameCont + str(iReg)
            StOptGlobal.Reference(p_ar, bellDump, str(p_iStep)).restore(0, self.m_continuationObj[iReg])
            
    def oneStep(self, p_statevector, p_phiInOut) :
        
        i = 0
        
        for i in range(len(p_statevector)) :
            self.m_pOptimize.stepSimulate(self.m_pGridFollowing, self.m_continuationObj, p_statevector[i], p_phiInOut[i])
