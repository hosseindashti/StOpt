// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#include <Eigen/Dense>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include "test/c++/tools/BasketOptions.h"
#include "StOpt/core/utils/comparisonUtils.h"
#include "test/c++/python/PayOffSwing.h"
#include "test/c++/python/PayOffFictitiousSwing.h"

/** \file BoostPythonUtils.cpp
 * \brief Map Pay off for swing
 * \author Xavier Warin
 */

using namespace Eigen;
using namespace std;
using namespace StOpt;

// #ifdef __linux__
// #ifdef __clang__
// // map std::shared ptr to boost python
// namespace boost
// {
// template<class T> T *get_pointer(std::shared_ptr<T> p)
// {
//     return p.get();
// }
// }
// #endif
// #endif

#ifdef _DEBUG
#undef _DEBUG
#include <Python.h>
#define _DEBUG
#else
#include <Python.h>
#endif
#include <numpy/arrayobject.h>
#include "StOpt/python/NumpyConverter.hpp"

using namespace boost::python;

//  map 0 function as a pay off function
class ZeroWrap: public StOpt::PayOffBase
{
public :

    // constructor
    ZeroWrap() {}

    /// \brief getFunction
    std::function<double(const int &, const Eigen::ArrayXd &, const Eigen::ArrayXd &)>  getFunction() const
    {
        auto f([](const int &, const Eigen::ArrayXd &, const Eigen::ArrayXd &)
        {
            return 0.;
        });
        return std::function<double(const int &, const Eigen::ArrayXd &, const Eigen::ArrayXd &)>(std::cref(f));
    }

    /// \brief  get back pay off function
    double set(const int &, const Eigen::ArrayXd &, const Eigen::ArrayXd &) const
    {
        return 0.;
    }
};

BOOST_PYTHON_MODULE(Utils)
{

    Register<ArrayXd>();

    class_<BasketCall>("BasketCall", init<const double & >())
    .def("applyVec", &BasketCall::applyVec)
    ;

    class_< PayOffSwing, bases< PayOffBase> >("PayOffSwing", init< const BasketCall &, const int & >())
    .def("set", &PayOffSwing::set)
    ;
    class_< PayOffFictitiousSwing, bases< PayOffBase> >("PayOffFictitiousSwing", init< const BasketCall &, const int & >())
    .def("set", &PayOffFictitiousSwing::set)
    ;
    class_<ZeroWrap, bases< PayOffBase> >("ZeroPayOff", init<>())
    .def("set", &ZeroWrap::set)
    ;

}
