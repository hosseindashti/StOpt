// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef AR1WRAP_H
#define AR1WRAP_H
#include "StOpt/core/grids/OneDimRegularSpaceGrid.h"
#include "StOpt/core/grids/OneDimData.h"
#include "StOpt/python/VectorAndList.h"
#include "test/c++/tools/simulators/AR1Simulator.h"
#include <Python.h>


/// \class AR1Wrap AR1Wrap.h
/// Simulator wrap
class AR1Wrap: public AR1Simulator
{
public :
    /// \brief  Constructor
    /// \param  p_D0     Initial value
    /// \param  p_m      average value
    /// \param  p_sigma  Volatility
    /// \param  p_mr     Mean reverting per factor
    /// \param  p_T      Maturity
    /// \param  p_nbStep   Number of time step for simulation
    /// \param  p_nbSimul  Number of simulations for the Monte Carlo
    /// \param  p_bForward true if the simulator is forward, false if the simulation is backward
    AR1Wrap(const double &p_D0, const double &p_m, const double     &p_sigma,   const double     &p_mr,
            const double &p_T,   const size_t &p_nbStep,  const size_t &p_nbSimul, const bool &p_bForward) :
        AR1Simulator(p_D0, p_m, p_sigma,   p_mr,  p_T,   p_nbStep,  p_nbSimul, p_bForward) {}
};

#endif /*  AR1WRAP_H */
