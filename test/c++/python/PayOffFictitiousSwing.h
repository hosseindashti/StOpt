// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef PAYOFFFICTITIOUSSWING_H
#define PAYOFFFICTITIOUSSWING_H
#include "StOpt/python/PayOffBase.h"
#include "test/c++/tools/BasketOptions.h"
#include "test/c++/tools/dp/FinalValueFictitiousFunction.h"

/** \file  PayOffFictitiousSwing.h
 * \author Xavier Warin
 */

/// \class  PayOffFictitiousSwing PayOffFictitiousSwing.h
/// Defines pay off for fictitious swing options
class PayOffFictitiousSwing: public StOpt::PayOffBase
{
private :

    std::function<double(const int &, const Eigen::ArrayXd &, const Eigen::ArrayXd &)> m_payOff;

public :

    /// \brief Constructor
    /// \param p_payOff  real pay off
    /// \param p_nExerc number of exercises for the swing
    PayOffFictitiousSwing(const BasketCall &p_payOff, const int &p_nExerc): m_payOff(FinalValueFictitiousFunction<BasketCall>(p_payOff, p_nExerc)) {}

    /// \brief getFunction
    /// \return get back the pay off function depending on the number of exercises already achieved
    std::function<double(const int &, const Eigen::ArrayXd &, const Eigen::ArrayXd &)>  getFunction() const
    {
        return m_payOff;
    }
    /// \brief  get back pay off function
    /// \param  p_iReg   regime number
    /// \param  p_stock  stock level
    /// \param  p_stochas   stochastic uncertainty
    double set(const int &p_iReg , const Eigen::ArrayXd &p_stock , const Eigen::ArrayXd &p_stochas) const
    {
        return m_payOff(p_iReg, p_stock, p_stochas);
    }

};
#endif

