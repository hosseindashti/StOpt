// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#include <Eigen/Dense>
#include <iostream>
#include <memory>
#include <boost/version.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>
#include "test/c++/tools/simulators/BlackScholesSimulator.h"

/** \file BoostPythonSimulators.cpp
 * \brief Map Black Scholes Simulators
 * \author Xavier Warin
 */


using namespace Eigen;
using namespace std;
using namespace StOpt;

#ifdef __linux__
#ifdef __clang__
#if BOOST_VERSION <  105600
// map std::shared ptr to boost python
namespace boost
{
template<class T> T *get_pointer(std::shared_ptr<T> p)
{
    return p.get();
}
}
#endif
#endif
#endif


#ifdef _DEBUG
#undef _DEBUG
#include <Python.h>
#define _DEBUG
#else
#include <Python.h>
#endif
#include <numpy/arrayobject.h>
#include "StOpt/python/NumpyConverter.hpp"
#include "test/c++/python/MeanRevertingAndFutureWrap.h"
#include "test/c++/python/OneDimDataWrap.h"
#include "test/c++/python/BlackScholesSimulatorWrap.h"
#include "test/c++/python/AR1Wrap.h"

using namespace boost::python;


/// \brief Encapsulation for simulators
BOOST_PYTHON_MODULE(Simulators)
{
    Register<MatrixXd>();
    Register<VectorXd>();

    ///  to map the constructor , should map  std::shared_ptr with boost python
    class_<BlackScholesSimulatorWrap,  std::shared_ptr< BlackScholesSimulatorWrap>, bases<SimulatorDPBase> >("BlackScholesSimulator", init<   const Eigen::VectorXd &, const Eigen::VectorXd &, const Eigen::VectorXd &,  const Eigen::MatrixXd &, const double &,  const size_t &, const size_t &,  const bool &   >())
    .def("getMu", &BlackScholesSimulatorWrap::getMu)
    ;

    class_<FutureCurve, std::shared_ptr<FutureCurve>>("FutureCurve", init< const boost::shared_ptr<OneDimRegularSpaceGrid>  & ,  const boost::python::list & >())
            ;

    class_<RegimeCurve, std::shared_ptr<RegimeCurve>>("RegimeCurve", init<const boost::shared_ptr<OneDimSpaceGrid>  & ,  const boost::python::list &>())
            ;

    class_<MeanRevertingWrap , std::shared_ptr< MeanRevertingWrap>, bases<SimulatorDPBase>  > ("MeanRevertingSimulator", init< const FutureCurve &, const Eigen::VectorXd &,   const Eigen::VectorXd &,
            const double &, const double &,   const size_t &,  const size_t &, const bool & >())
    ;

    class_<AR1Wrap , std::shared_ptr< AR1Wrap>, bases<SimulatorDPBase>  > ("AR1Simulator", init< const double, const double, const double &,   const double &,
            const double &,    const size_t &,  const size_t &, const bool & >())
    ;
}
