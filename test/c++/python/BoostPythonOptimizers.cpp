// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#include <Eigen/Dense>
#include <iostream>
#include <memory>
#include <boost/version.hpp>
#include <boost/bind.hpp>
#include "StOpt/core/grids/OneDimRegularSpaceGrid.h"
#include "StOpt/core/grids/OneDimData.h"
#include "test/c++/tools/simulators/MeanRevertingSimulator.h"
#include "test/c++/tools/simulators/AR1Simulator.h"
#include "test/c++/tools/BasketOptions.h"
#include "test/c++/tools/simulators/BlackScholesSimulator.h"
#include "test/c++/tools/dp/OptimizeSwing.h"
#include "test/c++/tools/dp/OptimizeFictitiousSwing.h"
#include "test/c++/tools/dp/OptimizeGasStorage.h"
#include "test/c++/tools/dp/OptimizeGasStorageSwitchingCost.h"
#include "test/c++/tools/dp/OptimizeLake.h"

/** \file BoostPythonOptimizers.cpp
 * \brief Map Optimizers for Swing
 * \author Xavier Warin
 */

using namespace Eigen;
using namespace std;
using namespace StOpt;


#ifdef __linux__
#ifdef __clang__
#if BOOST_VERSION <  105600
// map std::shared ptr to boost python
namespace boost
{
template<class T> T *get_pointer(std::shared_ptr<T> p)
{
    return p.get();
}
}
#endif
#endif
#endif

#ifdef _DEBUG
#undef _DEBUG
#include <Python.h>
#define _DEBUG
#else
#include <Python.h>
#endif
#include <numpy/arrayobject.h>
#include "StOpt/python/NumpyConverter.hpp"
#include "test/c++/python/MeanRevertingAndFutureWrap.h"
#include "test/c++/python/BlackScholesSimulatorWrap.h"
#include "test/c++/python/OneDimDataWrap.h"
#include "test/c++/python/AR1Wrap.h"



class OptimizerSwingBlackScholes : public  OptimizeSwing<BasketCall, BlackScholesSimulator>
{
public :
    OptimizerSwingBlackScholes(const BasketCall &p_payoff, const int &p_nPointStock): OptimizeSwing<BasketCall, BlackScholesSimulator>(p_payoff, p_nPointStock) {}

    inline void setSimulator(const std::shared_ptr<BlackScholesSimulatorWrap> &p_simulator)
    {
        OptimizeSwing::setSimulator(std::static_pointer_cast< BlackScholesSimulator >(p_simulator));
    }
};

class OptimizerFictitiousSwingBlackScholes : public  OptimizeFictitiousSwing<BasketCall, BlackScholesSimulator>
{
public :
    OptimizerFictitiousSwingBlackScholes(const BasketCall &p_payoff, const int &p_nPointStock, const int &p_ndim): OptimizeFictitiousSwing<BasketCall, BlackScholesSimulator>(p_payoff, p_nPointStock, p_ndim) {}

    inline void setSimulator(const std::shared_ptr<BlackScholesSimulatorWrap> &p_simulator)
    {
        OptimizeFictitiousSwing::setSimulator(std::static_pointer_cast< BlackScholesSimulator >(p_simulator));
    }
};


class OptimizeGasStorageMeanReverting : public OptimizeGasStorage< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > >
{
public :
    OptimizeGasStorageMeanReverting(const double   &p_injectionRate, const double &p_withdrawalRate ,  const double &p_injectionCost, const double &p_withdrawalCost):
        OptimizeGasStorage(p_injectionRate, p_withdrawalRate , p_injectionCost, p_withdrawalCost) {}

    inline void setSimulator(const std::shared_ptr<MeanRevertingWrap> &p_simulator)
    {
        OptimizeGasStorage:: setSimulator(std::static_pointer_cast< MeanRevertingSimulator< StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> > >(p_simulator));
    }
};

class OptimizeGasStorageSwitchingCostMeanReverting : public OptimizeGasStorageSwitchingCost< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > >
{
public :
    OptimizeGasStorageSwitchingCostMeanReverting(const double   &p_injectionRate, const double &p_withdrawalRate ,  const double &p_injectionCost, const double &p_withdrawalCost ,
            const double &p_switchCost):
        OptimizeGasStorageSwitchingCost(p_injectionRate, p_withdrawalRate , p_injectionCost, p_withdrawalCost,  p_switchCost) {}

    OptimizeGasStorageSwitchingCostMeanReverting(const double &p_injectionRate, const double &p_withdrawalRate,
            const double &p_injectionCost, const double &p_withdrawalCost,
            const double &p_switchCost,
            const boost::shared_ptr<RegimeCurve > &p_regime) :
        OptimizeGasStorageSwitchingCost(p_injectionRate, p_withdrawalRate, p_injectionCost, p_withdrawalCost, p_switchCost,
                                        make_shared_ptr<StOpt::OneDimData<StOpt::OneDimSpaceGrid, int> >(boost::static_pointer_cast<StOpt::OneDimData<StOpt::OneDimSpaceGrid, int> >(p_regime))) {}

    inline void setSimulator(const std::shared_ptr<MeanRevertingWrap> &p_simulator)
    {
        OptimizeGasStorageSwitchingCost:: setSimulator(std::static_pointer_cast< MeanRevertingSimulator< StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> > >(p_simulator));
    }
};

class OptimizeLakeAR1 : public OptimizeLake<AR1Simulator>
{
public :
    explicit OptimizeLakeAR1(const double &p_withdrawalRate) : OptimizeLake(p_withdrawalRate) {}

    inline void setSimulator(const std::shared_ptr<AR1Wrap> &p_simulator)
    {
        OptimizeLake::setSimulator(p_simulator);
    }
};

using namespace boost::python;

BOOST_PYTHON_MODULE(Optimizers)
{

    // map classical swing with Black Scholes
    class_<OptimizerSwingBlackScholes, std::shared_ptr<OptimizerSwingBlackScholes>, bases<OptimizerDPBase> > ("OptimizerSwingBlackScholes", init<const BasketCall &, const int & >())
    .def("setSimulator", &OptimizerSwingBlackScholes::setSimulator)
    ;
    // map  swing in nD  with Black Scholes
    class_<OptimizerFictitiousSwingBlackScholes, std::shared_ptr<OptimizerFictitiousSwingBlackScholes>, bases<OptimizerDPBase> > ("OptimizerFictitiousSwingBlackScholes", init<const BasketCall &, const int &, const int & >())
    .def("setSimulator", &OptimizerFictitiousSwingBlackScholes::setSimulator)
    ;
    // map gas storage
    class_<OptimizeGasStorageMeanReverting, std::shared_ptr<OptimizeGasStorageMeanReverting>, bases<OptimizerDPBase> >("OptimizeGasStorageMeanReverting", init<const double &,
            const double &,  const double &, const double & >())
    .def("setSimulator", &OptimizeGasStorageMeanReverting::setSimulator)
    ;
    // map gas storage with switching cost
    class_<OptimizeGasStorageSwitchingCostMeanReverting, std::shared_ptr<OptimizeGasStorageSwitchingCostMeanReverting>, bases<OptimizerDPBase> >("OptimizeGasStorageSwitchingCostMeanReverting", init<const double &,
            const double &,  const double &, const double &, const double & >())
    .def(init<const double &, const double &, const double &, const double &, const double &,  const boost::shared_ptr<RegimeCurve > &> ())
    .def("setSimulator", &OptimizeGasStorageSwitchingCostMeanReverting::setSimulator)
    ;
    // map lake
    class_<OptimizeLakeAR1, std::shared_ptr<OptimizeLakeAR1>, bases<OptimizerDPBase> >("OptimizeLakeAR1", init<const double &>())
    .def("setSimulator", &OptimizeLakeAR1::setSimulator)
    ;
}

