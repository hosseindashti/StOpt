// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef MEANREVERTINGANDFUTUREWRAP_H
#define  MEANREVERTINGANDFUTUREWRAP_H
#include "StOpt/core/grids/OneDimRegularSpaceGrid.h"
#include "StOpt/core/grids/OneDimData.h"
#include "StOpt/python/VectorAndList.h"
#include "test/c++/tools/simulators/MeanRevertingSimulator.h"
#include <Python.h>

/** \file MeanRevertingAndFutureWrap.h
 *  Defines the wrapping for future object and Mean Reverting Simulator
 * \author Xavier Warin
 */

/// \brief utility
template<typename T>
void do_release(typename boost::shared_ptr<T> const &, T *)
{
}

/// \brief Convert boost shared_ptr to std shared_ptr
template<typename T>
typename std::shared_ptr<T> make_shared_ptr(typename boost::shared_ptr<T> const &p)
{
    return
        std::shared_ptr<T>(
            p.get(),
            boost::bind(&do_release<T>, p, _1));

}

/// \class FutureCurve MeanRevertingAndFutureWrap.h
/// Future curve wrap
class FutureCurve : public  OneDimData<OneDimRegularSpaceGrid, double>
{
public :
    /// \brief constructor
    /// \param time    \f$x\f$  values
    /// \param values   \f$y\f$ values associated
    FutureCurve(const boost::shared_ptr<StOpt::OneDimRegularSpaceGrid> &time , const boost::python::list &values): StOpt::OneDimData<StOpt::OneDimRegularSpaceGrid, double>(make_shared_ptr<StOpt::OneDimRegularSpaceGrid>(time) , convertFromListToShared<double>(values)) {}
};


/// \class MeanRevertingWrap MeanRevertingAndFutureWrap.h
/// Simulator wrap
class MeanRevertingWrap: public MeanRevertingSimulator< StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> >
{
public :
    /// \brief  Constructor
    /// \param curve    future curve given
    /// \param p_sigma  Volatility vector
    /// \param p_mr     Mean reverting coefficient
    /// \param p_r      Interest rate
    /// \param p_T      Maturity
    /// \param p_nbStep     Number of time step to simulate
    /// \param p_nbSimul    Number of Monte Carlo simulations
    /// \param p_bForward   is it a forward simulator (or a backward one)?
    MeanRevertingWrap(const FutureCurve &curve, const Eigen::VectorXd    &p_sigma,   const Eigen::VectorXd    &p_mr,  const double &p_r,
                      const double &p_T,   const size_t &p_nbStep,  const size_t &p_nbSimul, const bool &p_bForward) :
        MeanRevertingSimulator< StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> >(std::make_shared< StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> >(static_cast<StOpt::OneDimData< StOpt::OneDimRegularSpaceGrid, double> >(curve)), p_sigma,   p_mr, p_r,
                p_T,   p_nbStep,  p_nbSimul, p_bForward) {}
};

#endif /*MeanRevertingAndFutureWrap.h */
