// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef MEANREVERTINGSIMULATORSDDP_H
#define MEANREVERTINGSIMULATORSDDP_H
#include <memory>
#include <boost/random.hpp>
#include "StOpt/core/utils/constant.h"
#include "StOpt/dp/SimulatorDPBase.h"
#include "StOpt/sddp/SimulatorSDDPBase.h"
#include  "test/c++/tools/simulators/MeanRevertingSimulator.h"


/* \file MeanRevertingSimulatorWithParticles.h
 * \brief Simulate a future deformation  as in MeanRevertingSimulator.h"
 * it  permits to generate some particles for sampling SDDP
 * \author Xavier Warin
 */


/// \class MeanRevertingSimulatorSDDP MeanRevertingSimulatorSDDP.h
/// Ornstein Uhlenbeck simulator
template< class Curve>
class MeanRevertingSimulatorSDDP: public MeanRevertingSimulator<Curve>
{
private :
    int m_dim ; ///< number of uncertainties (inflows, demand  for special SDDP treatment)
    int m_nbSample ; ///< number of samples
    Eigen::ArrayXXd m_particles ; ///< particles to generate (m_dim by m_nbSample) for sampling

public:

/// \brief Constructor
/// \param  p_curve  Initial forward curve
/// \param  p_sigma  Volatility of each factor
/// \param  p_mr     Mean reverting per factor
/// \param  p_T      Maturity
/// \param  p_nbStep Number of time step for simulation
/// \param p_nbSimul Number of simulations for the Monte Carlo
/// \param p_dim     dimension number (inflows, demand for SDDP)
/// \param p_nbSample number of simulations to sample for SDDP part
/// \param p_bForward true if the simulator is forward, false if the simulation is backward
    MeanRevertingSimulatorSDDP(std::shared_ptr<Curve> &p_curve,
                               const Eigen::VectorXd   &p_sigma,
                               const Eigen::VectorXd    &p_mr,
                               const double &p_T,
                               const size_t &p_nbStep,
                               const size_t &p_nbSimul,
                               const int   &p_dim,
                               const int &p_nbSample,
                               const bool &p_bForward):
        MeanRevertingSimulator<Curve>(p_curve, p_sigma, p_mr, 0., p_T, p_nbStep, p_nbSimul, p_bForward),
        m_dim(p_dim), m_nbSample(p_nbSample), m_particles(p_dim, p_nbSample)
    {
        // if backward, particle for expectation generated ones
        if (!p_bForward)
        {
            for (int is = 0; is < m_nbSample; ++is)
                for (int id = 0; id < m_dim; ++id)
                    m_particles(id, is) = MeanRevertingSimulator<Curve>::m_normalRand();
        }
    }
    ///\brief special function associated to the simulator
    /// \param p_idim  uncertainty targeted
    /// \param p_isim  simulation number
    inline double getGaussian(const int &p_idim, const int &p_isim)
    {
        return m_particles(p_idim, p_isim);
    }

    /// \brief Get back the number of samples
    inline int getNbSample() const
    {
        return m_nbSample;
    }

    /// \brief forward or backward update
    /// \param p_date  current date in simulator
    void updateDates(const double &p_date)
    {
        if (MeanRevertingSimulator<Curve>::m_bForward)
        {
            for (int is = 0; is < m_nbSample; ++is)
                for (int id = 0; id < m_dim; ++id)
                    m_particles(id, is) = MeanRevertingSimulator<Curve>::m_normalRand();
        }
        MeanRevertingSimulator<Curve>::updateDates(p_date);
    }



    /// \brief  update the number of simulations (forward only)
    /// \param p_nbSimul  Number of simulations to update
    /// \param p_nbSample Number of sample to update, useless here
    inline void updateSimulationNumberAndResetTime(const int &p_nbSimul, const int &p_nbSample)
    {
        m_nbSample = p_nbSample;
        m_particles.resize(m_dim, m_nbSample);
        MeanRevertingSimulator<Curve>::updateSimulationNumberAndResetTime(p_nbSimul, p_nbSample);
    }

};
#endif /* MEANREVERTINGSIMULATORSDDP_H */
