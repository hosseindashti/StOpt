// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef SIMUDPNONEMISSIVE_H
#define SIMUDPNONEMISSIVE_H
#include "geners/BinaryFileArchive.hh"
#include "StOpt/core/grids/SpaceGrid.h"
#include "StOpt/core/utils/StateWithStocks.h"
#include "StOpt/dp/OptimizerDPBase.h"
#include "StOpt/dp/SimulatorDPBase.h"

/** \file simuDPNonEmissive.h
 *  \brief Defines a simple program showing how to use simulation in a parallel framework
 *         Here Dynamic Programming methods are used to simulate
 *          Aid, Ren, Touzi :
 *        "Transition to non-emissive electricity production under optimal subsidy and endogenous carbon price"
 *        A simple grid  is used
 *  \author Xavier Warin
 */


/// \brief Simulate the optimal strategy , mpi version
/// \param p_grid                   grid used for  deterministic state (stocks for example)
/// \param p_optimize               optimizer defining the optimisation between two time steps
/// \param p_funcFinalValue         function defining the final value
/// \param p_pointStock             initial point stock
/// \param p_fileToDump             name associated to dumped bellman values
/// \param p_nbSimTostore           number of simulations to store
double simuDPNonEmissive(const std::shared_ptr<StOpt::SpaceGrid> &p_grid,
                         const std::shared_ptr<StOpt::OptimizerDPBase > &p_optimize,
                         const std::function<double(const int &, const Eigen::ArrayXd &, const Eigen::ArrayXd &)>  &p_funcFinalValue,
                         const Eigen::ArrayXd &p_pointStock,
                         const std::string   &p_fileToDump,
                         const int  &p_nbSimTostore);
#endif /* SIMUDPNONEMISSIVE_H */
